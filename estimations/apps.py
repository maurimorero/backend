from __future__ import unicode_literals

from django.apps import AppConfig


class EstimationsConfig(AppConfig):
    name = 'estimations'
    verbose_name = 'Cotizaciones'
