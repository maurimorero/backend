# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-05-22 02:03
from __future__ import unicode_literals

from django.db import migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('estimations', '0004_auto_20170409_0316'),
    ]

    operations = [
        migrations.AddField(
            model_name='customestimation',
            name='image_1',
            field=easy_thumbnails.fields.ThumbnailerImageField(blank=True, null=True, upload_to='static/image'),
        ),
        migrations.AddField(
            model_name='customestimation',
            name='image_2',
            field=easy_thumbnails.fields.ThumbnailerImageField(blank=True, null=True, upload_to='static/image'),
        ),
    ]
