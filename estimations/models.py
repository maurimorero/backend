# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from products.models import Product, Category, SubCategory
from users.models import AuctionUser

import datetime


class Quotation(models.Model):
    NO_OFFERS = 'NO'
    WITH_OFFERS = 'WO'
    SUCCESSFULLY = 'S'
    CANCELED = 'C'

    state = models.CharField(max_length=2, choices=(
        (NO_OFFERS, 'Without offers'),
        (WITH_OFFERS, 'With offers'),
        (SUCCESSFULLY, 'Successfully'),
        (CANCELED, 'Cancelled')
    ), default=NO_OFFERS, verbose_name='estado')

    date_created = models.DateField(auto_now_add=True, verbose_name='fecha')
    date_end = models.DateField(verbose_name='fecha fin')
    date_successfully = models.DateField(verbose_name='fecha de conversión',
                                         null=True, blank=True)
    count = models.IntegerField(verbose_name='cantidad')
    buyer = models.ForeignKey(AuctionUser, related_name='quotations',
                              verbose_name='comprador')
    comments = models.TextField(null=True, blank=True,
                                verbose_name='comentarios')

    def product(self):
        if hasattr(self, 'estimation'):
            return self.estimation.product.name
        return self.customestimation.requested_product
    product.short_description = 'Producto'

    def category(self):
        if hasattr(self, 'estimation'):
            return self.estimation.product.category
        return self.customestimation.category
    category.short_description = 'Categoría'

    def sub_category(self):
        if hasattr(self, 'estimation'):
            return self.estimation.product.sub_category
        return self.customestimation.sub_category
    sub_category.short_description = 'Sub-categoría'

    def offers(self):
        if hasattr(self, 'estimation'):
            return self.estimation.offers.all().count()
        return self.customestimation.offers.all().count()
    offers.short_description = 'Cantidad ofertas'

    def questions(self):
        if hasattr(self, 'estimation'):
            return self.estimation.questions.all().count()
        return self.customestimation.questions.all().count()
    questions.short_description = 'Cantidad preguntas'

    def buyer_full_name(self):
        return self.buyer.get_full_name()
    buyer_full_name.short_description = 'Comprador nombre'

    def is_custom_estimation(self):
        if hasattr(self, 'estimation'):
            return False
        return True

    class Meta:
        verbose_name = 'Cotización'
        verbose_name_plural = 'Cotizaciones'

    def __unicode__(self):
        return str(self.id)


class Estimation(Quotation):
    product = models.ForeignKey(Product, verbose_name='producto')

    def active(self):
        return self.date_end > datetime.date.today() and self.state in ['NO', 'WO']

    class Meta:
        verbose_name = 'Cotización simple/genérica'
        verbose_name_plural = 'Cotizaciones simples/genericas'

    def __unicode__(self):
        return "Producto: {0} comprador: {1}".format(
            self.product.name, str(self.buyer))


class CustomEstimation(Quotation):
    requested_product = models.CharField(max_length=200,
                                         verbose_name='producto solicitado')
    category = models.ForeignKey(Category, verbose_name='categoría')
    sub_category = models.ForeignKey(SubCategory, verbose_name='sub-categoría')

    def active(self):
        return self.date_end > datetime.date.today() and self.state in ['NO', 'WO']

    class Meta:
        verbose_name = 'Cotización personalizada'
        verbose_name_plural = 'Cotizaciones personalizadas'

    def __unicode__(self):
        return "Producto solicitado: {0} comprador: {1}".format(
            self.requested_product, str(self.buyer))


class Offer(models.Model):
    seller = models.ForeignKey(AuctionUser, related_name='offers',
                               verbose_name='vendedor')
    estimation = models.ForeignKey(Estimation, related_name='offers',
                                   null=True, blank=True,
                                   verbose_name='cotización simple/genérica')
    custom_estimation = models.ForeignKey(
        CustomEstimation, related_name='offers', null=True,
        blank=True, verbose_name='cotización personalizada'
    )
    date_created = models.DateField(auto_now_add=True, verbose_name='fecha')
    price = models.DecimalField(max_digits=10, decimal_places=2,
                                verbose_name='precio')
    description = models.CharField(max_length=255, null=True, blank=True,
                                   verbose_name='descripción')
    modified = models.DateField(auto_now=True,
                                verbose_name='fecha de modificación')
    canceled = models.BooleanField(default=False)

    def get_quoatation(self):
        if self.estimation:
            return self.estimation
        elif self.custom_estimation:
            return self.custom_estimation

    class Meta:
        verbose_name = 'Oferta'
        verbose_name_plural = 'Ofertas'

    def __unicode__(self):
        return "Cotización: {0} seller: {1}".format(
            self.estimation, str(self.seller))


class Question(models.Model):
    date_created = models.DateField(auto_now_add=True, verbose_name='fecha')
    user = models.ForeignKey(AuctionUser, verbose_name='usuario')
    estimation = models.ForeignKey(
        Estimation, null=True, blank=True, related_name='questions',
        verbose_name='cotización simple/genérica'
    )
    custom_estimation = models.ForeignKey(
        CustomEstimation, null=True, blank=True, related_name='questions',
        verbose_name='cotización personalizada'
    )
    question = models.CharField(max_length=255, verbose_name='pregunta')

    class Meta:
        verbose_name = 'Pregunta'
        verbose_name_plural = 'Preguntas'

    def __unicode__(self):
        return self.question


class Reply(models.Model):
    date_created = models.DateField(auto_now_add=True)
    user = models.ForeignKey(AuctionUser)
    answer = models.CharField(max_length=255)
    question = models.ForeignKey(Question, related_name='replies')

    def __unicode__(self):
        return self.answer
