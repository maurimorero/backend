# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.forms.models import model_to_dict
from django.core.files import File
from django.core.files.base import ContentFile
from django.db.models import Q

from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication
from tastypie.exceptions import BadRequest

from estimations.models import *
from products.models import *
from users.models import AuctionUser
from activitylog.models import UserActionLog

import datetime
import base64


class EstimationResource(ModelResource):
    product = fields.CharField(attribute='product')
    product_id = fields.IntegerField(attribute='product__id')

    category = fields.CharField(attribute='product__category')
    category_id = fields.IntegerField(attribute='product__category__id')

    sub_category = fields.CharField(attribute='product__sub_category')
    sub_category_id = fields.IntegerField(
        attribute='product__sub_category__id'
    )

    product_description = fields.CharField()
    buyer_city = fields.CharField()
    buyer_street = fields.CharField()
    buyer_reputation = fields.CharField()
    buyer_street_number = fields.CharField()

    buyer = fields.CharField(attribute='buyer')
    buyer_id = fields.IntegerField(attribute='buyer__id')

    is_active = fields.BooleanField(readonly=True)

    def dehydrate_product_description(self, bundle):
        if bundle.obj.product.description:
            return bundle.obj.product.description
        return ""

    def dehydrate_buyer_city(self, bundle):
        if bundle.obj.buyer.city:
            return bundle.obj.buyer.city
        return ""

    def dehydrate_buyer_street(self, bundle):
        if bundle.obj.buyer.street:
            return bundle.obj.buyer.street
        return ""

    def dehydrate_buyer_street_number(self, bundle):
        if bundle.obj.buyer.street:
            return bundle.obj.buyer.street_number
        return ""

    def dehydrate_buyer_reputation(self, bundle):
        return bundle.obj.buyer.buyer_reputation()

    def dehydrate_is_active(self, bundle):
        return bundle.obj.active()

    def dehydrate(self, bundle):
        # Add the existing offers of the estimations
        offers = []
        for offer in bundle.obj.offers.filter(canceled=False):
            offers.append(
                {
                    'id': offer.id,
                    'seller': offer.seller.get_full_name(),
                    'seller_id': offer.seller.id,
                    'seller_reputation': offer.seller.seller_reputation(),
                    'seller_count_sales': offer.seller.sales.all().count(),
                    'price': offer.price,
                    'modified': offer.modified,
                    'description': offer.description,
                    'location': ""  # FIXME
                }
            )

        bundle.data['offers'] = offers
        bundle.data['thumb_url_1'] = bundle.obj.product.get_thumb_1()
        bundle.data['thumb_url_2'] = bundle.obj.product.get_thumb_2()
        bundle.data['thumb_url_3'] = bundle.obj.product.get_thumb_3()
        bundle.data['thumb_url_4'] = bundle.obj.product.get_thumb_4()
        bundle.data['thumb_url_5'] = bundle.obj.product.get_thumb_5()
        bundle.data['thumb_url_6'] = bundle.obj.product.get_thumb_6()
        bundle.data['default_image'] = bundle.obj.product.sub_category.get_default_image()

        bundle.data['image_1'] = bundle.obj.product.image_1
        bundle.data['image_2'] = bundle.obj.product.image_2
        bundle.data['image_3'] = bundle.obj.product.image_3
        bundle.data['image_4'] = bundle.obj.product.image_4
        bundle.data['image_5'] = bundle.obj.product.image_5
        bundle.data['image_6'] = bundle.obj.product.image_6

        return bundle

    class Meta:
        queryset = Estimation.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'estimation'
        filtering = {
            'active': ALL,
            'buyer_id': ALL,
            'category_id': ALL,
            'sub_category_id': ALL
        }
        ordering = ['date_created', 'date_end']

    #def get_object_list(self, request):
    #    return super(EstimationResource, self).get_object_list(
    #        request).filter(date_end__gte=datetime.date.today())

    def build_filters(self, filters=None, **kwargs):
        if filters is None:
            filters = {}

        orm_filters = super(EstimationResource, self).build_filters(filters)

        # Filter by active
        if 'active' in filters and filters.get('active') == 'True':
            qset = Estimation.objects.filter(
                date_end__gte=datetime.date.today()
            )
            qset = qset.filter(state__in=[
                Estimation.NO_OFFERS, Estimation.WITH_OFFERS
            ])
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        # Filter by inactive
        if 'active' in filters and filters.get('active') == 'False':
            qset = Estimation.objects.filter(
                date_end__lte=datetime.date.today()
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'with-offers' in filters and filters.get('with-offers') == 'True':
            qset = Estimation.objects.filter(state=Estimation.WITH_OFFERS)
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'with-offers' in filters and filters.get('with-offers') == 'False':
            qset = Estimation.objects.filter(state=Estimation.NO_OFFERS)
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'expired' in filters and filters.get('expired') == 'True':
            qset = Estimation.objects.filter(
                date_end__lt=datetime.date.today()
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'expired' in filters and filters.get('expired') == 'False':
            qset = Estimation.objects.filter(
                date_end__gte=datetime.date.today()
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'successful' in filters and filters.get('successful') == 'True':
            qset = Estimation.objects.filter(state=Estimation.SUCCESSFULLY)
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'canceled' in filters and filters.get('canceled') == 'True':
            qset = Estimation.objects.filter(state=Estimation.CANCELED)
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'product-name' in filters:
            qset = Estimation.objects.filter(
                product__name__contains=filters.get("product-name"))
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'attrs_id' in filters:
            ids = filters.get('attrs_id').split(',')

            attributes = AttributeValue.objects.filter(
                attribute__id=ids[0],
            ).filter(product__isnull=False)

            product_ids = [attr.product.id for attr in attributes]

            for i in range(1, len(ids)):
                attributes = AttributeValue.objects.filter(
                    attribute__id=ids[i]).filter(product__isnull=False)
                product_ids = list(set(product_ids).intersection(
                        [attr.product.id for attr in attributes]))

            qset = Estimation.objects.filter(
                product__id__in=product_ids
            )

            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.id for i in qset]

        return orm_filters

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/create-generic-estimation%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_generic_estimation'),
                name="api_create_generic_estimation"
            ),
            url(
                r"^(?P<resource_name>%s)/estimation-create-offer%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_offer'),
                name="api_estimation_create_offer"
            )
        ]

    def create_generic_estimation(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        product_id = data.get('product_id')
        buyer_id = data.get('buyer_id')
        date_end = data.get('date_end')
        count = data.get('count')
        comments = data.get('comments')

        estimation = Estimation()
        estimation.product = Product.objects.get(id=product_id)
        estimation.buyer = AuctionUser.objects.get(id=buyer_id)
        estimation.date_end = date_end
        estimation.count = count
        estimation.comments = comments

        estimation.save()

        UserActionLog(
            user=request.user,
            action=UserActionLog.NEW_QUATATION,
            description="Cotización ID: %s" % estimation.id
        ).save()

        return self.create_response(request, {})

    def create_offer(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        seller_id = data.get('seller_id')
        estimation_id = data.get('estimation_id')
        price = data.get('price')
        description = data.get('description')

        seller = AuctionUser.objects.get(id=seller_id)
        estimation = Estimation.objects.get(id=estimation_id)
        estimation.state = Estimation.WITH_OFFERS  # With offers.
        estimation.save()

        try:
            offer = Offer.objects.get(
                seller=seller, estimation=estimation,
                canceled=False
            )
            offer.price = price
            offer.description = description
            offer.save()
        except Offer.DoesNotExist:
            offer = Offer()
            offer.seller = seller
            offer.estimation = estimation
            offer.price = price
            offer.description = description
            offer.save()

        UserActionLog(
            user=request.user,
            action=UserActionLog.NEW_OFFER,
            description="Oferta ID: %s" % offer.id
        ).save()

        return self.create_response(request, {})


class CustomEstimationResource(ModelResource):
    category = fields.CharField(attribute='category')
    category_id = fields.IntegerField(attribute='category__id')

    sub_category = fields.CharField(attribute='sub_category')
    sub_category_id = fields.IntegerField(attribute='sub_category__id')

    product_description = fields.CharField()

    buyer = fields.CharField(attribute='buyer')
    buyer_id = fields.IntegerField(attribute='buyer__id')

    buyer_city = fields.CharField()
    buyer_street = fields.CharField()
    buyer_street_number = fields.CharField()
    buyer_reputation = fields.CharField()

    is_active = fields.BooleanField(readonly=True)

    def dehydrate_is_active(self, bundle):
        return bundle.obj.active()

    def dehydrate_buyer_city(self, bundle):
        if bundle.obj.buyer.city:
            return bundle.obj.buyer.city
        return ""

    def dehydrate_buyer_street(self, bundle):
        if bundle.obj.buyer.street:
            return bundle.obj.buyer.street
        return ""

    def dehydrate_buyer_street_number(self, bundle):
        if bundle.obj.buyer.street:
            return bundle.obj.buyer.street_number
        return ""

    def dehydrate_buyer_reputation(self, bundle):
        return bundle.obj.buyer.buyer_reputation()

    def dehydrate(self, bundle):
        # Add the existing offers of the custom estimations
        offers = []
        for offer in bundle.obj.offers.filter(canceled=False):
            offers.append(
                {
                    'id': offer.id,
                    'seller': offer.seller.get_full_name(),
                    'seller_id': offer.seller.id,
                    'seller_reputation': offer.seller.seller_reputation(),
                    'seller_count_sales': offer.seller.sales.all().count(),
                    'price': offer.price,
                    'modified': offer.modified,
                    'description': offer.description,
                    'location': ""  # FIXME
                }
            )

        attributes = []
        for value in bundle.obj.attribute_values.all():
            attributes.append(
                {
                    'name': value.attribute.name,
                    'value': value.value,
                    'feature': value.attribute.feature.name,
                    'group': value.attribute.feature.group.name
                }
            )

        bundle.data['offers'] = offers
        bundle.data['default_image'] = bundle.obj.sub_category.get_default_image()
        bundle.data['attributes'] = attributes

        return bundle

    class Meta:
        queryset = CustomEstimation.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'custom-estimation'
        filtering = {
            'active': ALL,
            'buyer_id': ALL,
            'category_id': ALL,
            'sub_category_id': ALL
        }
        ordering = ['date_created', 'date_end']

    #def get_object_list(self, request):
    #    return super(CustomEstimationResource, self).get_object_list(
    #        request).filter(date_end__gte=datetime.date.today())

    def build_filters(self, filters=None, **kwargs):
        if filters is None:
            filters = {}

        orm_filters = super(CustomEstimationResource, self).build_filters(
            filters)

        # Filter by active
        if 'active' in filters and filters.get('active') == 'True':
            qset = CustomEstimation.objects.filter(
                date_end__gt=datetime.date.today()
            )
            qset = qset.filter(state__in=[
                Estimation.WITH_OFFERS, Estimation.NO_OFFERS
            ])
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        # Filter by inactive
        if 'active' in filters and filters.get('active') == 'False':
            qset = CustomEstimation.objects.filter(
                date_end__lte=datetime.date.today()
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'with-offers' in filters and filters.get('with-offers') == 'True':
            qset = CustomEstimation.objects.filter(
                state=Estimation.WITH_OFFERS
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'with-offers' in filters and filters.get('with-offers') == 'False':
            qset = CustomEstimation.objects.filter(state=Estimation.NO_OFFERS)
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'expired' in filters and filters.get('expired') == 'True':
            qset = CustomEstimation.objects.filter(
                date_end__lt=datetime.date.today()
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'expired' in filters and filters.get('expired') == 'False':
            qset = CustomEstimation.objects.filter(
                date_end__gte=datetime.date.today()
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'successful' in filters and filters.get('successful') == 'True':
            qset = CustomEstimation.objects.filter(
                state=Estimation.SUCCESSFULLY
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'canceled' in filters and filters.get('canceled') == 'True':
            qset = CustomEstimation.objects.filter(state=Estimation.CANCELED)
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'product-name' in filters:
            qset = CustomEstimation.objects.filter(
                requested_product__contains=filters.get("product-name"))
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'attrs_id' in filters:
            ids = filters.get('attrs_id').split(',')

            attributes = AttributeValue.objects.filter(
                attribute__id=ids[0],
            ).filter(custom_estimation__isnull=False)

            ce_ids = [attr.custom_estimation.id for attr in attributes]

            for i in range(1, len(ids)):
                attributes = AttributeValue.objects.filter(
                    attribute__id=ids[i]).filter(
                    custom_estimation__isnull=False)
                ce_ids = list(set(ce_ids).intersection(
                        [attr.custom_estimation.id for attr in attributes]))

            qset = CustomEstimation.objects.filter(
                id__in=ce_ids
            )

            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.id for i in qset]

        return orm_filters

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/create-custom-estimation%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_custom_estimation'),
                name="api_create_custom_estimation"
            ),
            url(
                r"^(?P<resource_name>%s)/custom-estimation-create-offer%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_offer'),
                name="api_custom_estimation_create_offer"
            ),
            url(
                r"^(?P<resource_name>%s)/get-estimation-attributes/(?P<id>[0-9]+)%s" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_estimation_attributes'),
                name="api_get_product_attributes"
            ),
        ]

    def create_custom_estimation(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        requested_product = data.get('requested_product')
        category_id = data.get('category_id')
        sub_category_id = data.get('sub_category_id')
        buyer_id = data.get('buyer_id')
        date_end = data.get('date_end')
        count = data.get('count')

        custom_estimation = CustomEstimation()
        custom_estimation.requested_product = requested_product
        custom_estimation.category = Category.objects.get(id=category_id)
        custom_estimation.sub_category = SubCategory.objects.get(id=sub_category_id)
        custom_estimation.buyer = AuctionUser.objects.get(id=buyer_id)
        custom_estimation.date_end = date_end
        custom_estimation.count = count

        custom_estimation.save()

        attrs = data.get('attrs')

        for attr in attrs:
            attr_value = AttributeValue()
            attr_value.value = attr['value']
            attr_value.attribute = Attribute.objects.get(id=attr['attribute_id'])
            attr_value.custom_estimation = custom_estimation
            attr_value.save()

        custom_estimation.comments = data.get('comments', '')
        custom_estimation.save()

        UserActionLog(
            user=custom_estimation.buyer,
            action=UserActionLog.NEW_QUATATION,
            description="Cotización ID: %s" % custom_estimation.id
        ).save()

        return self.create_response(request, {'id': custom_estimation.id})

    def get_estimation_attributes(self, request, id, **kwargs):
        self.method_check(request, allowed=['get'])
        # if not request.user.is_authenticated():
        #    raise BadRequest("Authorization error")

        sub_category = SubCategory.objects.get(id=id)
        result = []

        features = sub_category.features.all()

        for feature in features:
            for attr in feature.attributes.all():
                attr_data = model_to_dict(attr)
                attr_data['feature'] = feature.name
                attr_data['group'] = feature.group.name
                result.append(attr_data)

        return self.create_response(request, result)

    def create_offer(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        seller_id = data.get('seller_id')
        estimation_id = data.get('estimation_id')
        price = data.get('price')
        description = data.get('description')

        seller = AuctionUser.objects.get(id=seller_id)
        custom_estimation = CustomEstimation.objects.get(id=estimation_id)
        custom_estimation.state = Estimation.WITH_OFFERS
        custom_estimation.save()

        try:
            offer = Offer.objects.get(
                seller=seller, custom_estimation=custom_estimation,
                canceled=False
            )
            offer.price = price
            offer.description = description
            offer.save()
        except Offer.DoesNotExist:
            offer = Offer()
            offer.seller = seller
            offer.custom_estimation = custom_estimation
            offer.price = price
            offer.description = description
            offer.save()

        UserActionLog(
            user=request.user,
            action=UserActionLog.NEW_OFFER,
            description="Oferta ID: %s" % offer.id
        ).save()

        return self.create_response(request, {})


class QuestionResource(ModelResource):
    estimation_id = fields.IntegerField(attribute='estimation__id', null=True)
    custom_estimation_id = fields.IntegerField(
        attribute='custom_estimation__id', null=True)
    user = fields.CharField(attribute='user')

    def dehydrate(self, bundle):
        # Add the responses of the answer
        replies = []
        for reply in bundle.obj.replies.all():
            replies.append(
                {
                    'id': reply.id,
                    'user': reply.user,
                    'date_created': reply.date_created,
                    'answer': reply.answer
                }
            )

        bundle.data['replies'] = replies
        return bundle

    class Meta:
        queryset = Question.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'question'
        # authentication = SessionAuthentication()
        filtering = {
            'estimation_id': ALL,
            'custom_estimation_id': ALL,
            'user': ALL,
        }
        ordering = ['date_created']

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/create-question%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_question'),
                name="api_create_question"
            ),
            url(
                r"^(?P<resource_name>%s)/create-reply%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_reply'),
                name="api_create_reply"
            ),
            url(
                r"^(?P<resource_name>%s)/questions-received%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('questions_received'),
                name="api_questions_received"
            )
        ]

    def create_question(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )
        estimation_id = data.get('estimation_id', '')
        custom_estimation_id = data.get('custom_estimation_id', '')

        question = Question()
        if estimation_id:
            question.estimation = Estimation.objects.get(id=estimation_id)
        else:
            question.custom_estimation = CustomEstimation.objects.get(
                id=custom_estimation_id
            )
        question.user = request.user
        question.question = data.get('question')
        question.save()

        UserActionLog(
            user=request.user,
            action=UserActionLog.QUESTION,
            description="Pregunta ID: %s" % question.id
        ).save()

        return self.create_response(request, {})

    def create_reply(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        question_id = data.get('question_id')

        reply = Reply()
        reply.question = Question.objects.get(id=question_id)
        reply.user = request.user
        reply.answer = data.get('answer')
        reply.save()

        return self.create_response(request, {})

    def questions_received(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        user = request.user
        ordering = request.GET.get('ordering')

        quotations = Quotation.objects.filter(buyer=user)
        quotations_ids = [quotation.id for quotation in quotations]
        questions = Question.objects.filter(estimation__id__in=quotations_ids)

        if ordering and ordering == '-date_created':
            questions = questions.order_by('-date_created')
        elif ordering and ordering == 'date_created':
            questions = questions.order_by('date_created')

        result = []

        for question in questions:
            data = model_to_dict(question)
            data['date_created'] = question.date_created
            result.append(data)

        return self.create_response(request, result)


class OfferResource(ModelResource):
    class Meta:
        queryset = Offer.objects.all()
        allowed_methods = ['get']
        resource_name = 'offer'
        authentication = SessionAuthentication()
        filtering = {
            'state': ALL,
            'category': ALL
        }
        ordering = ['date_created', 'price']

    def get_object_list(self, request):
        return super(OfferResource, self).get_object_list(
            request).filter(seller__id=request.user.id, canceled=False)


    def dehydrate(self, bundle):
        offer = bundle.obj
        estimation = offer.estimation
        custom_estimation = offer.custom_estimation

        bundle.data['estimation'] = ""
        bundle.data['custom_estimation'] = ""

        if estimation:
            bundle.data['buyer'] = estimation.buyer.username
            bundle.data['product_name'] = estimation.product.name
            bundle.data['estimation'] = model_to_dict(estimation)
            bundle.data['category'] = estimation.product.category
            bundle.data['category_id'] = estimation.product.category.id
            bundle.data['sub_category'] = estimation.product.sub_category
            bundle.data['sub_category_id'] = estimation.product.sub_category.id
            bundle.data['thumb_url_1'] = estimation.product.get_thumb_1()
            bundle.data['thumb_url_2'] = estimation.product.get_thumb_2()
            bundle.data['thumb_url_3'] = estimation.product.get_thumb_3()
            bundle.data['thumb_url_4'] = estimation.product.get_thumb_4()
            bundle.data['thumb_url_5'] = estimation.product.get_thumb_5()
            bundle.data['thumb_url_6'] = estimation.product.get_thumb_6()
            bundle.data['default_image'] = estimation.product.sub_category.get_default_image()

        if custom_estimation:
            bundle.data['buyer'] = custom_estimation.buyer.username
            bundle.data['product_name'] = custom_estimation.requested_product
            bundle.data['custom_estimation'] = model_to_dict(custom_estimation)
            bundle.data['category'] = custom_estimation.category
            bundle.data['category_id'] = custom_estimation.category.id
            bundle.data['sub_category'] = custom_estimation.sub_category
            bundle.data['sub_category_id'] = custom_estimation.sub_category.id
            bundle.data['default_image'] = custom_estimation.sub_category.get_default_image()

        return bundle

    def build_filters(self, filters=None, **kwargs):
        if filters is None:
            filters = {}

        orm_filters = super(OfferResource, self).build_filters(
            filters)

        if 'state' in filters:
            qset = Offer.objects.filter(
                Q(estimation__state=filters.get('state')) |
                Q(custom_estimation__state=filters.get('state'))
            )
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'category' in filters:
            qset = Offer.objects.filter(
                Q(estimation__product__category__id=filters.get('category')) |
                Q(custom_estimation__category__id=filters.get('category'))
            )
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'in-progress' in filters:
            qset = Offer.objects.filter(
                Q(estimation__state__in=['WO', 'NO']) |
                Q(custom_estimation__state__in=['WO', 'NO'])
            )
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'successfully' in filters:
            qset = Offer.objects.filter(
                Q(estimation__state='S') |
                Q(custom_estimation__state='S')
            )
            orm_filters['pk__in'] = [i.pk for i in qset]

        if 'expired' in filters:
            qset = Offer.objects.filter(
                Q(estimation__date_end__lt=datetime.date.today()) |
                Q(custom_estimation__date_end__lt=datetime.date.today())
            )
            orm_filters['pk__in'] = [i.pk for i in qset]

        return orm_filters

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/cancel-offer/(?P<id>[0-9]+)%s" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('cancel_offer'),
                name="api_cancel_offer"
            ),
        ]

    def cancel_offer(self, request, id, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        offer = Offer.objects.get(id=id)
        offer.canceled = True
        offer.save()

        return self.create_response(request, {})
