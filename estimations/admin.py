from django.contrib import admin

from rangefilter.filter import DateRangeFilter

from estimations.models import *
from products.models import AttributeValue


class AttributeValueInline(admin.TabularInline):
    fields = ("attribute", "value")

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(AttributeValueInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'attribute':
            if hasattr(request, '_obj_'):
                field.queryset = field.queryset.filter(
                    sub_category=request._obj_.sub_category
                )
            else:
                field.queryset = field.queryset.none()
        return field

    model = AttributeValue


class EstimationAdmin(admin.ModelAdmin):
    list_display = ('product', 'date_created', 'date_end', 'buyer')
    list_filter = ('date_created', 'date_end', 'product__category',
                   'product__sub_category')
    search_fields = ['product__name', 'buyer__username']


class CustomEstimationAdmin(admin.ModelAdmin):
    inlines = [
        AttributeValueInline,
    ]
    list_display = ('requested_product', 'date_created', 'date_end', 'buyer')
    list_filter = ('date_created', 'date_end', 'category', 'sub_category')
    search_fields = ['buyer__username']


class OfferAdmin(admin.ModelAdmin):
    list_display = ('estimation', 'custom_estimation', 'date_created',
                    'seller', 'price')
    list_filter = (('date_created', DateRangeFilter), 'estimation',
                   'custom_estimation', 'seller')
    search_fields = ['seller__first_name', 'seller__last_name',
                     'seller__username', 'description',
                     'custom_estimation__requested_product',
                     'estimation__product__name']


class ReplyInline(admin.TabularInline):
    model = Reply


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('estimation', 'custom_estimation', 'date_created', 'user')
    search_fields = ['question']
    list_filter = ('estimation', 'custom_estimation', 'user',
                   ('date_created', DateRangeFilter))
    inlines = [
        ReplyInline,
    ]


class QuotationAdmin(admin.ModelAdmin):
    list_display = ('id', 'product', 'date_created', 'buyer',
                    'buyer_full_name', 'category', 'sub_category', 'count',
                    'date_end', 'offers', 'questions')
    list_filter = (('date_created', DateRangeFilter),
                   ('date_end', DateRangeFilter), 'buyer')
    search_fields = ['buyer__username', 'buyer__first_name',
                     'buyer__last_name', 'estimation__product__name',
                     'customestimation__requested_product']

    def product(self, obj):
        product_name = None
        url = None
        if not obj.is_custom_estimation():
            product_name = obj.estimation.product.name
            url = "/admin/products/product/%s/change/" % str(
                obj.estimation.product.id)
        else:
            url = "/admin/estimations/customestimation/%s/change/" % (
                obj.customestimation.id)
            product_name = obj.customestimation.requested_product
        return u'<a href="%s">%s</a>' % (url, product_name)
    product.allow_tags = True
    product.short_description = "producto"

    def offers(self, obj):
        url = "/admin/estimations/offer/"
        if not obj.is_custom_estimation():
            url += "?estimation__quotation_ptr__exact=" + str(obj.id)
        else:
            url += "?estimation__quotation_ptr__exact=" + str(obj.id)
        return u'<a href="%s">%s</a>' % (url, obj.offers())
    offers.allow_tags = True
    offers.short_description = "cantidad ofertas"

    def questions(self, obj):
        url = "/admin/estimations/question/"
        if not obj.is_custom_estimation():
            url += "?custom_estimation__quotation_ptr__exact=" + str(obj.id)
        else:
            url += "?estimation__quotation_ptr__exact=" + str(obj.id)
        return u'<a href="%s">%s</a>' % (url, obj.questions())
    questions.allow_tags = True
    questions.short_description = "cantidad preguntas"

admin.site.register(Estimation, EstimationAdmin)
admin.site.register(CustomEstimation, CustomEstimationAdmin)
admin.site.register(Offer, OfferAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Quotation, QuotationAdmin)
