from django.contrib import admin
# from django import forms

from rangefilter.filter import DateRangeFilter

from products.models import *


class AttributeValueInline(admin.TabularInline):
    exclude = ("custom_estimation",)
    fields = ("attribute", "value")

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(AttributeValueInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'attribute':
            if request._obj_ is not None:
                field.queryset = field.queryset.filter(
                    feature__sub_category=request._obj_.sub_category
                )
            else:
                field.queryset = field.queryset.none()
        return field

    model = AttributeValue


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ('name',)


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'category')
    list_filter = ('category',)
    search_fields = ('name',)


class ProductAdmin(admin.ModelAdmin):
    inlines = [
        AttributeValueInline,
    ]
    list_display = ('id', 'date_created', 'name', 'category', 'sub_category',
                    'count_prices')
    list_filter = (('date_created', DateRangeFilter), 'category',
                   'sub_category')
    search_fields = ('name',)

    class Media:
        js = ('js/filter.js',)

    def get_form(self, request, obj=None, **kwargs):
        request._obj_ = obj
        return super(ProductAdmin, self).get_form(request, obj, **kwargs)


class PriceAdmin(admin.ModelAdmin):
    list_display = ('product', 'price')


class UserPreferencesAdmin(admin.ModelAdmin):
    search_fields = ('user__username', 'user__first_name', 'user__last_name')


class FeatureAdmin(admin.ModelAdmin):
    list_display = ('id', 'group', 'name')
    list_filter = ('group',)
    search_fields = ('name',)


class AttributeAdmin(admin.ModelAdmin):
    list_display = ('id', 'group_name', 'feature_name', 'name')
    list_filter = ('feature__group', )
    search_fields = ('name',)


class AttributeValueAdmin(admin.ModelAdmin):
    list_display = ('product', 'attribute', 'product_value')
    list_filter = ('attribute',)
    search_fields = ('value', 'product__name')


admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Price, PriceAdmin)
admin.site.register(Attribute, AttributeAdmin)
admin.site.register(AttributeValue, AttributeValueAdmin)
admin.site.register(FeatureGroup)
admin.site.register(Feature, FeatureAdmin)
