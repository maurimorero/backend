# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.forms.models import model_to_dict
from django.core.files import File
from django.core.files.base import ContentFile

from tastypie.exceptions import BadRequest
from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication

from products.models import *
from users.models import AuctionUser
from activitylog.models import UserActionLog

import base64


class CategoryResource(ModelResource):
    class Meta:
        queryset = Category.objects.all()
        allowed_methods = ['get']
        resource_name = 'category'
        filtering = {
            'name': ALL
        }


class SubCategoryResource(ModelResource):
    category = fields.CharField(attribute='category')
    category_id = fields.IntegerField(attribute='category__id')

    class Meta:
        queryset = SubCategory.objects.all()
        allowed_methods = ['get']
        resource_name = 'subcategory'
        filtering = {
            'name': ALL,
            'category_id': ALL
        }


class ProductResource(ModelResource):
    user = fields.CharField(attribute='user', default='')
    category = fields.CharField(attribute='category')
    category_id = fields.IntegerField(attribute='category__id')
    sub_category = fields.CharField(attribute='sub_category')
    sub_category_id = fields.IntegerField(attribute='sub_category__id')
    request_status = fields.CharField()

    def dehydrate_request_status(self, bundle):
        return bundle.obj.get_request_status_display()

    class Meta:
        queryset = Product.objects.all()
        allowed_methods = ['get']
        resource_name = 'product'
        filtering = {
            'user_id': ALL,
            'category_id': ALL,
            'sub_category_id': ALL,
            'name': ALL,
            'user': ALL
        }
        ordering = ['name', 'date_created', 'lowest_price', 'bigest_price']

    def get_object_list(self, request):
        qset = super(ProductResource, self).get_object_list(request).filter(
            request_status='A').exclude(prices__isnull=True)

        if request.user.is_authenticated():
            my_products = Product.objects.filter(user=request.user)
            return qset | my_products

        return qset

    def dehydrate(self, bundle):
        prices = []
        for price in Price.objects.filter(product__id=bundle.obj.id):
            prices.append(
                {
                    'id': price.id,
                    'seller_name': price.seller.get_full_name(),
                    'seller_id': price.seller.id,
                    'seller_reputation': price.seller.seller_reputation(),
                    'seller_count_sales': price.seller.sales.all().count(),
                    'price': price.price,
                    'stock': price.stock,
                    'price_modified': price.modified
                }
            )

        attributes = []
        for value in bundle.obj.attribute_values.all():
            attributes.append(
                {
                    'name': value.attribute.name,
                    'value': value.value,
                    'feature': value.attribute.feature.name,
                    'group': value.attribute.feature.group.name
                }
            )

        bundle.data['prices'] = prices
        bundle.data['attributes'] = attributes
        bundle.data['thumb_url_1'] = bundle.obj.get_thumb_1()
        bundle.data['thumb_url_2'] = bundle.obj.get_thumb_2()
        bundle.data['thumb_url_3'] = bundle.obj.get_thumb_3()
        bundle.data['thumb_url_4'] = bundle.obj.get_thumb_4()
        bundle.data['thumb_url_5'] = bundle.obj.get_thumb_5()
        bundle.data['thumb_url_6'] = bundle.obj.get_thumb_6()
        bundle.data['default_image'] = bundle.obj.sub_category.get_default_image()

        if bundle.request.user.id:
            bundle.data['favorite'] = Favorite.objects.filter(
                user=bundle.request.user, product=bundle.obj).exists()
        else:
            bundle.data['favorite'] = False

        return bundle

    def build_filters(self, filters=None, **kwargs):
        if filters is None:
            filters = {}

        orm_filters = super(ProductResource, self).build_filters(filters)

        # Filter by user preferences
        if 'user_id' in filters:
            # Get the user preferences
            user = AuctionUser.objects.get(id=filters.get('user_id'))
            preferences, created = UserPreferences.objects.get_or_create(
                user=user)
            preferences = preferences.categories.all()
            # Get the ids of preferences
            preference_ids = [preference.id for preference in preferences]

            qset = Product.objects.filter(
                category__id__in=preference_ids
            )
            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.id for i in qset]

        # Filter by attributes
        if 'attrs_id' in filters:
            ids = filters.get('attrs_id').split(',')

            attributes = AttributeValue.objects.filter(
                attribute__id=ids[0],
            ).filter(product__isnull=False)

            product_ids = [attr.product.id for attr in attributes]

            for i in range(1, len(ids)):
                attributes = AttributeValue.objects.filter(
                    attribute__id=ids[i]).filter(product__isnull=False)
                product_ids = list(set(product_ids).intersection(
                        [attr.product.id for attr in attributes]))

            qset = Product.objects.filter(
                id__in=product_ids
            )

            if 'pk__in' in orm_filters:
                qset = qset.filter(pk__in=orm_filters['pk__in'])
            orm_filters['pk__in'] = [i.id for i in qset]

        return orm_filters

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/set-price%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('set_price'),
                name="api_set_price"
            ),
            url(
                r"^(?P<resource_name>%s)/delete-price%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('delete_price'),
                name="api_delete_price"
            ),
            url(
                r"^(?P<resource_name>%s)/get-user-products%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_user_products'),
                name="api_get_user_products"
            ),
            url(
                r"^(?P<resource_name>%s)/set-product-as-favorite%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('set_product_as_favorite'),
                name="api_set_product_as_favorite"
            ),
            url(
                r"^(?P<resource_name>%s)/get-favorite-products%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_favorite_products'),
                name="api_get_favorite_products"
            ),
            url(
                r"^(?P<resource_name>%s)/load-product%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('load_product'),
                name="api_load_product"
            ),
            url(
                r"^(?P<resource_name>%s)/get-product-attributes/(?P<id>[0-9]+)%s" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_product_attributes'),
                name="api_get_product_attributes"
            ),
            url(
                r"^(?P<resource_name>%s)/get-attributes%s" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_attributes'),
                name="api_get_attributes"
            ),
            url(
                r"^(?P<resource_name>%s)/set-product-attributes/(?P<id>[0-9]+)%s" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('set_product_attributes'),
                name="api_set_product_attributes"
            )
        ]

    def set_price(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        seller_id = data.get('seller_id')
        product_id = data.get('product_id')
        seller_price = data.get('price')
        stock = data.get('stock')

        seller = AuctionUser.objects.get(id=seller_id)
        product = Product.objects.get(id=product_id)

        try:
            price = Price.objects.get(seller=seller, product=product)
        except Price.DoesNotExist:
            price = Price()
            price.seller = seller
            price.product = product

        price.price = seller_price
        price.stock = stock
        price.save()

        prices = Price.objects.filter(
            product__id=product_id).values_list('price', flat=True)
        product.lowest_price = min(prices)
        product.bigest_price = max(prices)
        product.save()

        UserActionLog(
            user=request.user,
            action=UserActionLog.NEW_PRICE,
            description="Precio ID: %s" % price.id
        ).save()

        return self.create_response(request, {})

    def delete_price(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        price_id = data.get('price_id')

        price = Price.objects.get(id=price_id)

        if not price.seller == request.user:
            raise BadRequest("Authorization error")

        price.delete()

        UserActionLog(
            user=request.user,
            action=UserActionLog.DELETE_PRICE,
            description="Precio eliminado"
        ).save()

        return self.create_response(request, {})

    def set_product_as_favorite(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        product_id = data.get('product_id')
        product = Product.objects.get(id=product_id)
        favorite, created = Favorite.objects.get_or_create(
            product=product, user=request.user)
        if created:
            favorite.save()
        else:
            favorite.delete()

        return self.create_response(request, {})

    def get_favorite_products(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        query_string = request.GET.get('search', '')
        ids = request.GET.get('attrs_id', '')
        order_by = request.GET.get('order_by', '')

        favorites = None

        if query_string:
            favorites = Favorite.objects.filter(
                user=request.user,
                product__name__contains=query_string
            )
        else:
            favorites = Favorite.objects.filter(user=request.user)

        if ids:  # Filter by attr ids
            ids = ids.split(',')
            attributes = AttributeValue.objects.filter(
                attribute__id=ids[0],
            ).filter(product__isnull=False)

            product_ids = [attr.product.id for attr in attributes]

            for i in range(1, len(ids)):
                attributes = AttributeValue.objects.filter(
                    attribute__id=ids[i]).filter(product__isnull=False)
                product_ids = list(set(product_ids).intersection(
                        [attr.product.id for attr in attributes]))

            favorites = favorites.filter(product__id__in=product_ids)

        if order_by and order_by == 'lowest_price':
            favorites = favorites.order_by('product__lowest_price')
        elif order_by and order_by == 'bigest_price':
            favorites = favorites.order_by('product__bigest_price')
        else:
            favorites = favorites.order_by('-product__date_created')

        result = []

        for e in favorites:
            product = e.product
            product_data = model_to_dict(product)
            product_data['category'] = product.category
            product_data['category_id'] = product.category.id
            product_data['sub_category'] = product.sub_category
            product_data['sub_category_id'] = product.sub_category.id
            product_data['thumb_url_1'] = product.get_thumb_1()
            product_data['thumb_url_2'] = product.get_thumb_2()
            product_data['thumb_url_3'] = product.get_thumb_3()
            product_data['thumb_url_4'] = product.get_thumb_4()
            product_data['thumb_url_5'] = product.get_thumb_5()
            product_data['thumb_url_6'] = product.get_thumb_6()
            product_data['default_image'] = product.sub_category.get_default_image()
            product_data['favorite'] = True
            prices = []
            product_prices = e.product.prices.all()
            for price in product_prices:
                prices.append(
                    {
                        'id': price.id,
                        'seller_name': price.seller.get_full_name(),
                        'seller_id': price.seller.id,
                        'seller_reputation': price.seller.seller_reputation(),
                        'seller_count_sales': price.seller.sales.all().count(),
                        'price': price.price,
                        'stock': price.stock,
                        'price_modified': price.modified
                    }
                )
            product_data['prices'] = prices
            result.append(product_data)

        return self.create_response(request, result)

    def get_user_products(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        status = request.GET.get("filter", None)
        order = request.GET.get("order", None)
        products = None

        if status:
            products = Price.objects.filter(
                seller=request.user, product__request_status=status
            )
        else:
            products = Price.objects.filter(seller=request.user)

        if order and order == 'lowest_price':
            products = products.order_by('product__lowest_price')
        elif order and order == 'bigest_price':
            products = products.order_by('-product__bigest_price')
        elif order and order == 'recent':
            products = products.order_by('-product__date_created')
        elif order and order == 'older':
            products = products.order_by('product__date_created')

        result = []

        for product in products:
            product_data = model_to_dict(product.product)
            product_data['price'] = product.price
            product_data['stock'] = product.stock
            product_data['price_modified'] = product.modified
            product_data['category'] = product.product.category
            product_data['category_id'] = product.product.category.id
            product_data['sub_category'] = product.product.sub_category
            product_data['sub_category_id'] = product.product.sub_category.id
            product_data['thumb_url_1'] = product.product.get_thumb_1()
            product_data['thumb_url_2'] = product.product.get_thumb_2()
            product_data['thumb_url_3'] = product.product.get_thumb_3()
            product_data['thumb_url_4'] = product.product.get_thumb_4()
            product_data['thumb_url_5'] = product.product.get_thumb_5()
            product_data['thumb_url_6'] = product.product.get_thumb_6()
            product_data['default_image'] = product.product.sub_category.get_default_image()
            result.append(product_data)

        return self.create_response(request, result)

    def load_product(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        category_id = data.get('category_id')
        sub_category_id = data.get('sub_category_id')
        name = data.get('name')
        state = data.get('state')
        price = data.get('price')
        stock = data.get('stock', '')

        image1 = data.get('image_1', '')
        image2 = data.get('image_2', '')
        image3 = data.get('image_3', '')
        image4 = data.get('image_4', '')
        image5 = data.get('image_5', '')
        image6 = data.get('image_6', '')

        exist_product = Product.objects.filter(name=name,
                                               request_status='A')

        if exist_product:
            return self.create_response(request, {
                'id': exist_product[0].id, 'created': False
            })

        # Create product
        product = Product()
        product.user = request.user
        product.category = Category.objects.get(id=category_id)
        product.sub_category = SubCategory.objects.get(id=sub_category_id)
        product.name = name
        product.state = state

        product.save()

        # Create product price
        product_price = Price()
        product_price.seller = request.user
        product_price.product = product
        product_price.price = price
        product_price.stock = stock
        product_price.save()

        # Set images of the product
        if image1:
            format, imgstr = image1.split(';base64,')
            ext = format.split('/')[-1]
            file_name = str(product.id) + '_image_1.' + ext
            data = ContentFile(base64.b64decode(imgstr), name=file_name)
            product.image_1 = File(data)

        if image2:
            format, imgstr = image2.split(';base64,')
            ext = format.split('/')[-1]
            file_name = str(product.id) + '_image_2.' + ext
            data = ContentFile(base64.b64decode(imgstr), name=file_name)
            product.image_2 = File(data)

        if image3:
            format, imgstr = image3.split(';base64,')
            ext = format.split('/')[-1]
            file_name = str(product.id) + '_image_3.' + ext
            data = ContentFile(base64.b64decode(imgstr), name=file_name)
            product.image_3 = File(data)

        if image4:
            format, imgstr = image4.split(';base64,')
            ext = format.split('/')[-1]
            file_name = str(product.id) + '_image_4.' + ext
            data = ContentFile(base64.b64decode(imgstr), name=file_name)
            product.image_4 = File(data)

        if image5:
            format, imgstr = image5.split(';base64,')
            ext = format.split('/')[-1]
            file_name = str(product.id) + '_image_5.' + ext
            data = ContentFile(base64.b64decode(imgstr), name=file_name)
            product.image_5 = File(data)

        if image6:
            format, imgstr = image6.split(';base64,')
            ext = format.split('/')[-1]
            file_name = str(product.id) + '_image_6.' + ext
            data = ContentFile(base64.b64decode(imgstr), name=file_name)
            product.image_6 = File(data)

        product.save()

        return self.create_response(request, {
            'id': product.id, 'created': True})

    def get_product_attributes(self, request, id, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        product = Product.objects.get(id=id)
        result = []

        features = product.sub_category.features.all()

        for feature in features:
            for attr in feature.attributes.all():
                attr_data = model_to_dict(attr)
                attr_data['feature'] = feature.name
                attr_data['group'] = feature.group.name
                result.append(attr_data)

        return self.create_response(request, result)

    def get_attributes(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        # if not request.user.is_authenticated():
        #     raise BadRequest("Authorization error")

        products = Product.objects.all()
        result = []

        for product in products:
            features = product.sub_category.features.all()

            for feature in features:
                for attr in feature.attributes.all():
                    attr_data = model_to_dict(attr)
                    attr_data['feature'] = feature.name
                    attr_data['group'] = feature.group.name
                    result.append(attr_data)

        return self.create_response(request, result)

    def set_product_attributes(self, request, id, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        product = Product.objects.get(id=id)
        attrs = data.get('attrs')

        for attr in attrs:
            attr_value = AttributeValue()
            attr_value.value = attr['value']
            attr_value.attribute = Attribute.objects.get(
                id=attr['attribute_id'])
            attr_value.product = product
            attr_value.save()

        product.description = data.get('description', '')
        product.save()

        UserActionLog(
            user=request.user,
            action=UserActionLog.NEW_PRODUCT,
            description="Producto ID: %s" % product.id
        ).save()

        return self.create_response(request, {})


class UserPreferencesResource(ModelResource):
    categories = fields.ToManyField(CategoryResource, 'categories', full=True)

    class Meta:
        queryset = UserPreferences.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'user-preferences'
        authentication = SessionAuthentication()

    # def get_object_list(self, request):
    #     return UserPreferences.objects.filter(user=request.user)

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/update-user-preferences%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_user_preferences'),
                name="api_update_user_preferences"
            ),
            url(
                r"^(?P<resource_name>%s)/get-user-preferences%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_user_preferences'),
                name="api_get_user_preferences"
            )
        ]

    def get_user_preferences(self, request, **kwargs):
        user_preferences, created = UserPreferences.objects.get_or_create(
            user=request.user)

        data = model_to_dict(user_preferences)
        data["categories"] = []

        for category in user_preferences.categories.all():
            data["categories"].append(model_to_dict(category))

        return self.create_response(request, data)

    def update_user_preferences(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        category_ids = data.get('category_ids')

        user_preference, created = UserPreferences.objects.get_or_create(
            user=request.user)

        user_preference.categories.clear()

        for category_id in category_ids:
            user_preference.categories.add(
                Category.objects.get(id=category_id)
            )

        user_preference.save()

        return self.create_response(request, {})
