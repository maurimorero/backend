# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-07-09 23:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0010_auto_20170709_2125'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='stock',
        ),
        migrations.AddField(
            model_name='price',
            name='stock',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
