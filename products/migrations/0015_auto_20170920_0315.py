# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-09-20 03:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0014_auto_20170920_0312'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='approved',
        ),
        migrations.AddField(
            model_name='product',
            name='request_status',
            field=models.CharField(choices=[('A', 'Approved'), ('R', 'Rejected'), ('P', 'Pending')], default=False, max_length=1, verbose_name='estado'),
        ),
    ]
