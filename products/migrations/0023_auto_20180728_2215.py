# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2018-07-28 22:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0022_auto_20180501_2304'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='date_created',
            field=models.DateField(auto_now_add=True, verbose_name='fecha'),
        ),
    ]
