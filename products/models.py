# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models

from users.models import AuctionUser

from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.fields import ThumbnailerImageField


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='nombre')

    class Meta:
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorias'

    def __unicode__(self):
        return self.name


class SubCategory(models.Model):
    category = models.ForeignKey(Category, verbose_name='categoría')
    name = models.CharField(max_length=100, verbose_name='nombre')
    default_image = ThumbnailerImageField(
        upload_to="static/image",
        null=True,
        blank=True, verbose_name='imagen por defecto'
    )

    def get_default_image(self):
        thumb_url = ""
        if self.default_image:
            options = {'size': (340, 200), 'crop': True}
            thumb_url = get_thumbnailer(
                self.default_image).get_thumbnail(options).url
        return thumb_url

    class Meta:
        verbose_name = 'Sub-categoria'
        verbose_name_plural = 'Sub-categorias'

    def __unicode__(self):
        return self.name


class Product(models.Model):
    user = models.ForeignKey(AuctionUser, null=True, blank=True,
                             verbose_name='usuario')
    category = models.ForeignKey(Category, verbose_name='categoría')
    sub_category = models.ForeignKey(SubCategory, verbose_name='sub-categoría')
    name = models.CharField(max_length=100, verbose_name='nombre')
    description = models.TextField(null=True, blank=True,
                                   verbose_name='descripción')
    request_status = models.CharField(
        choices=(
            ('A', 'Aprobado'),
            ('R', 'Rechazado'),
            ('P', 'Pendiente')
        ),
        default='P', verbose_name='estado',
        max_length=1
    )
    state = models.CharField(choices=(('U', 'Used'), ('N', 'New')),
                             max_length=1, null=True, blank=True,
                             verbose_name='tipo de producto')
    image_1 = ThumbnailerImageField(
        upload_to="static/image",
        null=True,
        blank=True,
        verbose_name='imagen_1'
    )
    image_2 = ThumbnailerImageField(
        upload_to="static/image",
        null=True,
        blank=True,
        verbose_name='imagen_2'
    )
    image_3 = ThumbnailerImageField(
        upload_to="static/image",
        null=True,
        blank=True,
        verbose_name='imagen_3'
    )
    image_4 = ThumbnailerImageField(
        upload_to="static/image",
        null=True,
        blank=True,
        verbose_name='imagen_4'
    )
    image_5 = ThumbnailerImageField(
        upload_to="static/image",
        null=True,
        blank=True,
        verbose_name='imagen_5'
    )
    image_6 = ThumbnailerImageField(
        upload_to="static/image",
        null=True,
        blank=True,
        verbose_name='imagen_6'
    )

    date_created = models.DateField(auto_now_add=True, verbose_name='fecha')
    lowest_price = models.DecimalField(max_digits=10, decimal_places=2,
                                       null=True, blank=True)
    bigest_price = models.DecimalField(max_digits=10, decimal_places=2,
                                       null=True, blank=True)

    def get_thumb_1(self):
        thumb_url = ""
        if self.image_1:
            options = {'size': (340, 340), 'crop': True}
            thumb_url = get_thumbnailer(self.image_1).get_thumbnail(options).url
        return thumb_url

    def get_thumb_2(self):
        thumb_url = ""
        if self.image_2:
            options = {'size': (340, 340), 'crop': True}
            thumb_url = get_thumbnailer(self.image_2).get_thumbnail(options).url
        return thumb_url

    def get_thumb_3(self):
        thumb_url = ""
        if self.image_3:
            options = {'size': (340, 340), 'crop': True}
            thumb_url = get_thumbnailer(self.image_3).get_thumbnail(options).url
        return thumb_url

    def get_thumb_4(self):
        thumb_url = ""
        if self.image_4:
            options = {'size': (340, 340), 'crop': True}
            thumb_url = get_thumbnailer(self.image_4).get_thumbnail(options).url
        return thumb_url

    def get_thumb_5(self):
        thumb_url = ""
        if self.image_5:
            options = {'size': (340, 340), 'crop': True}
            thumb_url = get_thumbnailer(self.image_5).get_thumbnail(options).url
        return thumb_url

    def get_thumb_6(self):
        thumb_url = ""
        if self.image_6:
            options = {'size': (340, 340), 'crop': True}
            thumb_url = get_thumbnailer(self.image_6).get_thumbnail(options).url
        return thumb_url

    def count_prices(self):
        return self.prices.all().count()

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __unicode__(self):
        return self.name


class FeatureGroup(models.Model):
    name = models.CharField(max_length=100, verbose_name='nombre')

    class Meta:
        verbose_name = 'Grupo'
        verbose_name_plural = 'Grupos'

    def __unicode__(self):
        return self.name


class Feature(models.Model):
    name = models.CharField(max_length=100)
    group = models.ForeignKey(FeatureGroup, null=True, blank=False,
                              verbose_name='grupo')
    sub_category = models.ForeignKey(SubCategory, related_name='features',
                                     verbose_name='sub-categoría')

    class Meta:
        verbose_name = 'Funcionalidad'
        verbose_name_plural = 'Funcionalidades'

    def __unicode__(self):
        return ("%s - %s") % (self.group.name, self.name)


class Attribute(models.Model):
    feature = models.ForeignKey(Feature, null=True, blank=False,
                                verbose_name='caracteristica',
                                related_name='attributes')
    name = models.CharField(max_length=100, verbose_name='nombre')

    def group_name(self):
        if self.feature:
            return self.feature.group.name
        return "-"
    group_name.short_description = 'grupo'

    def feature_name(self):
        return self.feature.name
    feature_name.short_description = 'Funcionalidad'

    class Meta:
        verbose_name = 'Atributo'
        verbose_name_plural = 'Atributos'

    def __unicode__(self):
        return ("%s - %s - %s" % (self.group_name(), self.feature.name,
                                  self.name))


class AttributeValue(models.Model):
    value = models.NullBooleanField(verbose_name='valor', default=False)
    attribute = models.ForeignKey(Attribute, verbose_name='atributo')
    product = models.ForeignKey(Product, related_name='attribute_values',
                                null=True, blank=True, verbose_name='producto')
    custom_estimation = models.ForeignKey('estimations.CustomEstimation',
                                          related_name='attribute_values',
                                          null=True, blank=True,
                                          verbose_name='cotización personalida')

    def product_value(self):
        if self.value:
            return "Si"
        return "No"
    product_value.short_description = 'Poseé'

    class Meta:
        verbose_name = 'Valor del atributo'
        verbose_name_plural = 'Valores de atributos'

    def __unicode__(self):
        return str(self.id)


class Price(models.Model):
    seller = models.ForeignKey(AuctionUser, verbose_name='vendedor')
    product = models.ForeignKey(Product, related_name='prices',
                                verbose_name='producto')
    price = models.DecimalField(max_digits=10, decimal_places=2,
                                verbose_name='precio')
    modified = models.DateField(auto_now=True, verbose_name='modificado')
    stock = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = 'Precio'
        verbose_name_plural = 'Precios'

    def __unicode__(self):
        return "Producto: {0} precio: {1}".format(
            self.product.name, str(self.price))


class UserPreferences(models.Model):
    user = models.OneToOneField(AuctionUser)
    categories = models.ManyToManyField(Category)

    def __unicode__(self):
        return str(self.user)


class Favorite(models.Model):
    user = models.ForeignKey(AuctionUser, related_name='favorites')
    product = models.ForeignKey(Product)

    def __unicode__(self):
        return str(self.user)
