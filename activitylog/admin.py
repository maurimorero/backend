from django.contrib import admin

from rangefilter.filter import DateRangeFilter

from activitylog.models import *


class UserActionLogAdmin(admin.ModelAdmin):
    list_display = ('user', 'datetime', 'action', 'description')
    list_filter = (('datetime', DateRangeFilter), 'action')
    search_fields = ['user__username', 'description']

    def has_add_permission(self, request):
        return False


admin.site.register(UserActionLog, UserActionLogAdmin)
