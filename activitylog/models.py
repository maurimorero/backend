# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class UserActionLog(models.Model):
    SIGNUP = 'signup'
    LOGIN = 'login'
    LOGOUT = 'logout'
    PURCHASE = 'purchase'
    SALE = 'sale'
    QUESTION = 'question'
    NEW_PRODUCT = 'new-product'
    NEW_PRICE = 'new-price'
    DELETE_PRICE = 'delete-price'
    NEW_OFFER = 'new-offer'
    NEW_QUATATION = 'new-quoatation'

    ACTIONS_CHOICES = (
        (SIGNUP, 'Cuenta creada'),
        (LOGIN, 'Inicio de sesión'),
        (LOGOUT, 'Cierre de sesión'),
        (PURCHASE, 'Compra'),
        (SALE, 'Venta'),
        (QUESTION, 'Pregunta realizada'),
        (NEW_PRODUCT, 'Nuevo producto'),
        (NEW_PRICE, 'Nuevo precio'),
        (DELETE_PRICE, 'Precio eliminado'),
        (NEW_OFFER, 'Nueva oferta'),
        (NEW_QUATATION, 'Nueva cotización'),
    )

    user = models.ForeignKey('users.auctionuser', verbose_name='usuario')
    datetime = models.DateTimeField(auto_now_add=True, verbose_name='fecha')
    action = models.CharField(max_length=100, choices=ACTIONS_CHOICES,
                              verbose_name='acción')
    description = models.CharField(max_length=255, null=True, blank=True,
                                   verbose_name='descripción')

    class Meta:
        verbose_name = "Log"
        verbose_name_plural = "Logs"

    def __unicode__(self):
        return "Usuario: {0}, acción: {1}".format(
            self.user, self.action)
