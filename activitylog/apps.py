from __future__ import unicode_literals

from django.apps import AppConfig


class ActivitylogConfig(AppConfig):
    name = 'activitylog'
    verbose_name = 'Registro de actividades'
