# -*- coding: utf-8 -*-

from django.conf.urls import url

from tastypie.utils import trailing_slash
from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication
from tastypie.exceptions import BadRequest

from activitylog.models import UserActionLog
from users.models import AuctionUser
from purchases.models import Purchase


class UserActionLogResource(ModelResource):
    class Meta:
        queryset = UserActionLog.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'activitylog'
        authentication = SessionAuthentication()

    def obj_get_list(self, bundle, **kwargs):
        return UserActionLog.objects.filter(user__id=bundle.request.user.id)

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/user-action-logs%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('user_action_logs'),
                name="api_user_action_logs"
            )
        ]

    def user_action_logs(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        signup = UserActionLog.objects.filter(
            action=UserActionLog.SIGNUP).last()

        last_login = UserActionLog.objects.filter(
            action=UserActionLog.LOGIN).last()

        last_purchase = UserActionLog.objects.filter(
            action=UserActionLog.PURCHASE).last()

        last_sale = UserActionLog.objects.filter(
            action=UserActionLog.SALE).last()

        purchase_count = Purchase.objects.filter(seller=request.user).count()

        sales_count = Purchase.objects.filter(buyer=request.user).count()

        return self.create_response(request, {
            'signup': signup.datetime if signup else '',
            'last_login': last_login.datetime if last_login else '',
            'last_purchase': last_purchase.datetime if last_purchase else '',
            'purchase_count': purchase_count,
            'last_sale': last_sale.datetime if last_sale else '',
            'sales_count': sales_count
        })
