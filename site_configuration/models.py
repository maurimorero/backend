from __future__ import unicode_literals

from django.db import models


from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.fields import ThumbnailerImageField


class Header(models.Model):
    image_main = ThumbnailerImageField(
        upload_to="static/image",
        verbose_name='image main'
    )

    image_1 = ThumbnailerImageField(
        upload_to="static/image",
        verbose_name='image 1'
    )
    image_2 = ThumbnailerImageField(
        upload_to="static/image",
        verbose_name='image 2'
    )
    image_3 = ThumbnailerImageField(
        upload_to="static/image",
        verbose_name='image 3'
    )

    def get_image_main(self):
        options = {'size': (340, 200), 'crop': True}
        url = get_thumbnailer(
            self.image_main).get_thumbnail(options).url
        return url

    def get_image_1(self):
        options = {'size': (340, 200), 'crop': True}
        url = get_thumbnailer(
            self.image_1).get_thumbnail(options).url
        return url

    def get_image_2(self):
        options = {'size': (340, 200), 'crop': True}
        url = get_thumbnailer(
            self.image_2).get_thumbnail(options).url
        return url

    def get_image_3(self):
        options = {'size': (340, 200), 'crop': True}
        url = get_thumbnailer(
            self.image_3).get_thumbnail(options).url
        return url

    class Meta:
        verbose_name = 'Header'
        verbose_name_plural = 'Header'

    def __unicode__(self):
        return "Header"


class Contact(models.Model):
    address = models.CharField(max_length=255, null=True, blank=True)
    phone = models.CharField(max_length=255)
    email = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contacto'

    def __unicode__(self):
        address = self.address if self.address else ""
        return "%s %s %s" % (self.address, self.phone, self.email)

