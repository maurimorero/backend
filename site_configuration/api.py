# -*- coding: utf-8 -*-

from django.conf.urls import url

from tastypie.utils import trailing_slash
from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication
from tastypie.exceptions import BadRequest

from site_configuration.models import *


class SiteConfigurationResource(ModelResource):
    class Meta:
        queryset = Header.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'header'
        # authentication = SessionAuthentication()

    def obj_get_list(self, bundle, **kwargs):
        first = Header.objects.first()
        if first:
            return [first]
        return []

    def dehydrate(self, bundle):
        bundle.data['image_main_crop'] = bundle.obj.get_image_main()
        bundle.data['image_1_crop'] = bundle.obj.get_image_1()
        bundle.data['image_2_crop'] = bundle.obj.get_image_2()
        bundle.data['image_3_crop'] = bundle.obj.get_image_3()

        return bundle


class ContactResource(ModelResource):
    class Meta:
        queryset = Contact.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'contact'
        # authentication = SessionAuthentication()

    def obj_get_list(self, bundle, **kwargs):
        first = Contact.objects.first()
        if first:
            return [first]
        return []
