from django.contrib import admin

from site_configuration.models import *


admin.site.register(Header)
admin.site.register(Contact)
