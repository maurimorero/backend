# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SiteConfigurationConfig(AppConfig):
    name = 'site_configuration'
    verbose_name = 'Configuracion del sitio'
