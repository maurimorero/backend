from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from rangefilter.filter import DateRangeFilter

from users.models import AuctionUser


class AuctionUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = AuctionUser


class AuctionUserAdmin(UserAdmin):
    form = AuctionUserChangeForm

    fieldsets = UserAdmin.fieldsets + (
            (None, {
                'fields': ('gender', 'birthdate', 'document', 'cuit', 'city',
                           'street', 'street_number', 'floor', 'unit',
                           'area_code', 'phone', 'user_activated', 'photo',
                           'cuit_facturation', 'city_facturation',
                           'street_facturation', 'street_number_facturation',
                           'floor_facturation', 'unit_facturation',
                           'business_name_facturation', 'tax_type_facturation',
                           'ml_is_active')
            }),
    )
    list_display = ('id', 'first_name', 'last_name', 'document', 'username',
                    'phone', 'quotation_count', 'purchase_count', 'sale_count',
                    'offer_count', 'questions_count')

    list_filter = (("date_joined", DateRangeFilter),)

    search_fields = ['username', 'first_name', 'last_name', 'document']

    def quotation_count(self, obj):
        url = "/admin/estimations/quotation/?buyer__id__exact=" + str(obj.id)
        return u'<a href="%s">%s</a>' % (url, obj.quotation_count())
    quotation_count.allow_tags = True
    quotation_count.short_description = "cantidad cotizaciones"

    def purchase_count(self, obj):
        url = "/admin/purchases/purchase/?buyer__id__exact=" + str(obj.id)
        return u'<a href="%s">%s</a>' % (url, obj.purchase_count())
    purchase_count.allow_tags = True
    purchase_count.short_description = "cantidad compras"

    def sale_count(self, obj):
        url = "/admin/purchases/purchase/?seller__id__exact=" + str(obj.id)
        return u'<a href="%s">%s</a>' % (url, obj.sale_count())
    sale_count.allow_tags = True
    sale_count.short_description = "cantidad ventas"

    def offer_count(self, obj):
        url = "/admin/estimations/offer/?seller__id__exact=" + str(obj.id)
        return u'<a href="%s">%s</a>' % (url, obj.offer_count())
    offer_count.allow_tags = True
    offer_count.short_description = "cantidad ofertas"

    def questions_count(self, obj):
        url = "/admin/estimations/question/?user__id__exact=" + str(obj.id)
        return u'<a href="%s">%s</a>' % (url, obj.questions_count())
    questions_count.allow_tags = True
    questions_count.short_description = "cantidad preguntas"

admin.site.register(AuctionUser, AuctionUserAdmin)
