# -*- coding: utf-8 -*-

from django.contrib.auth import authenticate, login, logout
from django.conf.urls import url
from django.db import IntegrityError
from django.forms.models import model_to_dict
from django.core.mail import EmailMultiAlternatives
from django.contrib.sites.shortcuts import get_current_site

from tastypie.http import HttpUnauthorized, HttpForbidden
from tastypie import fields
from tastypie.utils import trailing_slash
from tastypie.resources import ModelResource
from tastypie.exceptions import BadRequest
from tastypie.authentication import SessionAuthentication

from users.models import AuctionUser
from activitylog.models import UserActionLog

from datetime import datetime

import requests
import uuid
from django.conf import settings


def send_activation_code(user, request):
    subject = 'MLA mail de activación'
    from_email = 'info@mla.com'
    to = user.email
    text_content = ''
    url_activation = 'http://{}/activate/{}/{}'.format(
        str(get_current_site(request).domain), user.id, user.activation_code
    )

    html_content = '<p>Para continuar siga el siguiente enlace:</p>\
    <a href={}>Activar cuenta</a>\
    <p>O copie y pegue en su navegador la siguiente url: {}</p>'.format(
        url_activation, url_activation)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def send_reset_code(user, request):
    subject = 'MLA mail de recuperación de contraseña'
    from_email = 'info@mla.com'
    to = user.email
    text_content = ''
    url_reset = 'http://{}/reset/{}'.format(
        str(get_current_site(request).domain), user.reset_code
    )

    html_content = '<p>Para continuar siga el siguiente enlace:</p>\
    <a href={}>Recuperar contraseña</a>\
    <p>O copie y pegue en su navegador la siguiente url: {}</p>'.format(
        url_reset, url_reset)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


class UserResource(ModelResource):
    tax_type_facturation = fields.CharField()
    url_authorization_ml = fields.CharField()
    is_authorized_by_ml = fields.CharField()

    def dehydrate_tax_type_facturation(self, bundle):
        return bundle.obj.tax_type_facturation

    def dehydrate_url_authorization_ml(self, bundle):
        url_redirect = 'https://mercadoalreves.com/profile'  # FIXME!
        url = "https://auth.mercadopago.com.ar/authorization?client_id=%s&response_type=code&platform_id=mp&redirect_uri=%s" % (
            settings.ML_APP_ID, url_redirect
        )

        return url

    def dehydrate_is_authorized_by_ml(self, bundle):
        if bundle.obj.ml_is_active and bundle.obj.ml_access_token:
            return True
        return False

    class Meta:
        queryset = AuctionUser.objects.all()
        allowed_methods = ['get', 'post', 'put']
        resource_name = 'user'
        # authentication = SessionAuthentication()

    def prepend_urls(self):
        return [
            url(r'^(?P<resource_name>%s)/signup%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('signup'), name='api_signup'),
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
            url(r'^(?P<resource_name>%s)/reputation/(?P<id>[0-9]+)%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('reputation'), name='api_reputation'),
            url(r'^(?P<resource_name>%s)/update-profile/(?P<id>[0-9]+)%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_profile'), name='api_update_profile'),
            url(r'^(?P<resource_name>%s)/change-password%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('change_password'), name='api_change_password'),
            url(r'^(?P<resource_name>%s)/activate-user%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('activate_user'), name='api_activate_user'),
            url(r'^(?P<resource_name>%s)/reset-user%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('reset_user'), name='api_reset_user'),
            url(r'^(?P<resource_name>%s)/reset-password%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('reset_password'), name='api_reset_password'),
            url(r'^(?P<resource_name>%s)/authorization-ml%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('authorization_ml'),
                name='api_authorization_ml'),
            url(r'^(?P<resource_name>%s)/inactive-ml%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('inactive_ml'),
                name='api_inactive_ml')
        ]

    def signup(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        if AuctionUser.objects.filter(email=data.get('email')):
            return self.create_response(
                request,
                {'error': 'Ya existe un usuario registrado con ese email'},
                HttpForbidden
            )

        try:
            user = AuctionUser.objects.create_user(
                data.get('username'),
                data.get('email'),
                data.get('password')
            )
            user.save()

            UserActionLog(user=user, action=UserActionLog.SIGNUP).save()
        except IntegrityError:
            raise BadRequest("Nombre de usuario no disponible")

        user.first_name = data.get('first_name')
        user.last_name = data.get('last_name')
        user.activation_code = uuid.uuid4().hex

        user.save()

        send_activation_code(user, request)

        return self.create_response(request, model_to_dict(user))

    def change_password(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        password = data.get('password')
        request.user.set_password(password)
        request.user.save()

        user = authenticate(username=request.user.username, password=password)
        login(request, user)

        return self.create_response(request, {})

    def activate_user(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        code = data.get('code')
        user_id = data.get('user_id')

        try:
            user = AuctionUser.objects.get(activation_code=code, id=user_id)
            user.user_activated = True
            user.save()

        except AuctionUser.DoesNotExist:
            return self.create_response(request,
                                        {'error': 'Usuario no encontrado'},
                                        HttpForbidden)

        return self.create_response(request, {})

    def reset_user(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        email = data.get('email')

        if AuctionUser.objects.filter(email=email).count() > 2:
            return self.create_response(
                request,
                {'error': 'Ocurrio un error al resetear la contraseña'},
                HttpForbidden
            )

        try:
            user = AuctionUser.objects.get(email=email)
            user.reset_code = uuid.uuid4().hex
            user.save()

        except AuctionUser.DoesNotExist:
            return self.create_response(request,
                                        {'error': 'Usuario no encontrado'},
                                        HttpForbidden)

        send_reset_code(user, request)

        return self.create_response(request, {})

    def reset_password(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        password = data.get('password')
        code = data.get('code')

        try:
            user = AuctionUser.objects.get(reset_code=code)

        except AuctionUser.DoesNotExist:
            return self.create_response(request,
                                        {'error': 'Usuario no encontrado'},
                                        HttpForbidden)
        user.set_password(password)
        user.save()

        return self.create_response(request, {})

    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        username = data.get('username', '')
        password = data.get('password', '')

        user = authenticate(username=username, password=password)

        if user:
            if user.user_activated:
                login(request, user)

                UserActionLog(user=user, action=UserActionLog.LOGIN).save()

                return self.create_response(request, model_to_dict(user))
            else:
                return self.create_response(request, {
                    'error': 'Primero debe activar su cuenta antes de continuar.',
                    }, HttpForbidden)
        else:
            return self.create_response(request, {
                'error': 'Contraseña o usuario incorrectos',
                }, HttpUnauthorized)

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get'])

        if request.user and request.user.is_authenticated():

            UserActionLog(
                user=request.user, action=UserActionLog.LOGOUT).save()

            logout(request)
            return self.create_response(request, {})
        else:
            return self.create_response(request, {})

    def reputation(self, request, id, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        user = AuctionUser.objects.get(id=id)

        return self.create_response(request, {
            'seller_reputation': user.seller_reputation(),
            'seller_comments': user.seller_comments(),
            'buyer_reputation': user.buyer_reputation(),
            'buyer_comments': user.buyer_comments(),
            'sales': user.sales.all().count(),
            'purchases': user.purchases.all().count()
        })

    def update_profile(self, request, id, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        user = AuctionUser.objects.get(id=id)

        # if data.get('first_name'):
        user.first_name = data.get('first_name')

        # if data.get('last_name'):
        user.last_name = data.get('last_name')

        # if data.get('email'):
        user.email = data.get('email')

        # if data.get('area_code'):
        user.area_code = data.get('area_code')

        # if data.get('city'):
        user.city = data.get('city')

        # if data.get('document'):
        user.document = data.get('document')

        # if data.get('floor'):
        user.floor = data.get('floor')

        # if data.get('gender'):
        user.gender = data.get('gender')

        # if data.get('phone'):
        user.phone = data.get('phone')

        # if data.get('street'):
        user.street = data.get('street')

        # if data.get('street_number'):
        user.street_number = data.get('street_number')

        # if data.get('unit'):
        user.unit = data.get('unit')

        # if data.get('cuit'):
        user.cuit = data.get('cuit')

        if data.get('birthdate') and not data.get('birthdate') == "Invalid date":
            user.birthdate = datetime.strptime(
                data.get('birthdate'), '%Y-%m-%d').date()
        else:
            user.birthdate = None

        # if data.get('cuit_facturation'):
        user.cuit_facturation = data.get('cuit_facturation')

        # if data.get('city_facturation'):
        user.city_facturation = data.get('city_facturation')

        # if data.get('street_facturation'):
        user.street_facturation = data.get('street_facturation')

        # if data.get('street_number_facturation'):
        user.street_number_facturation = data.get('street_number_facturation')

        # if data.get('floor_facturation'):
        user.floor_facturation = data.get('floor_facturation')

        # if data.get('unit_facturation'):
        user.unit_facturation = data.get('unit_facturation')

        user.business_name_facturation = data.get('business_name_facturation')

        user.tax_type_facturation = data.get('tax_type_facturation')

        user.save()

        return self.create_response(request, {})

    def authorization_ml(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        token = data.get('token')

        url = "https://api.mercadopago.com/oauth/token"

        response = requests.post(url, data={
            "client_id": settings.ML_APP_ID,
            "client_secret": settings.ML_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": token,
            "redirect_uri": 'https://mercadoalreves.com/profile'  # FIXME!
        })

        user = request.user
        user.ml_is_active = True
        user.ml_access_token = response.json()["access_token"]
        user.ml_refresh_token = response.json()["refresh_token"]
        user.save()

        return self.create_response(request, {})

    def inactive_ml(self, request, **kwargs):
        self.method_check(request, allowed=['get'])

        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        user = request.user
        user.ml_is_active = False
        user.save()

        return self.create_response(request, {})
