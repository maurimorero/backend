# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser


class AuctionUser(AbstractUser):
    first_name = models.CharField(max_length=100, null=True, blank=True,
                                  verbose_name='nombre')
    last_name = models.CharField(max_length=100, null=True, blank=True,
                                 verbose_name='apellido')
    gender = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        choices=(
            ('M', 'Male'),
            ('F', 'Female')
        ), verbose_name='sexo'
    )
    birthdate = models.DateField(null=True, blank=True,
                                 verbose_name='fecha de nacimiento')
    document = models.IntegerField(null=True, blank=True,
                                   verbose_name='documento')
    phone = models.IntegerField(null=True, blank=True, verbose_name='telefono')

    cuit = models.IntegerField(null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True,
                            verbose_name='ciudad')
    street = models.CharField(max_length=100, null=True, blank=True,
                              verbose_name='calle')
    street_number = models.IntegerField(null=True, blank=True,
                                        verbose_name='número')
    floor = models.IntegerField(null=True, blank=True, verbose_name='piso')
    unit = models.CharField(max_length=10, null=True, blank=True,
                            verbose_name='unidad')
    area_code = models.IntegerField(null=True, blank=True,
                                    verbose_name='código de area')
    ml_access_token = models.CharField(max_length=255, null=True, blank=True)
    ml_refresh_token = models.CharField(max_length=255, null=True, blank=True)
    ml_is_active = models.NullBooleanField(default=False,
                                           verbose_name="MercadoPago activado")

    # Datos de facuracion
    tax_type_facturation = models.CharField(
        choices=(
            ('I', 'Responsable Inscripto'),
            ('M', 'Monotributista'),
            ('E', 'Exento'),
            ('F', 'Consumidor Final')
        ),
        default='F', verbose_name='estado',
        max_length=1
    )
    business_name_facturation = models.CharField(
        max_length=100, null=True, blank=True
    )
    cuit_facturation = models.IntegerField(null=True, blank=True,
                                           verbose_name='cuit de facturación')
    city_facturation = models.CharField(max_length=100, null=True, blank=True,
                                        verbose_name='ciudad de facturación')
    street_facturation = models.CharField(
        max_length=100, null=True, blank=True,
        verbose_name='Calle de facturación'
    )
    street_number_facturation = models.IntegerField(
        null=True, blank=True, verbose_name='número de facturación'
    )
    floor_facturation = models.IntegerField(null=True, blank=True,
                                            verbose_name='piso de facturación')
    unit_facturation = models.CharField(max_length=10, null=True, blank=True,
                                        verbose_name='unidad de facturación')

    photo = models.ImageField(upload_to="photo", null=True, blank=True,
                              verbose_name='foto')

    user_activated = models.BooleanField(default=True,
                                         verbose_name='usuario activado')
    activation_code = models.CharField(max_length=255, null=True, blank=True,
                                       verbose_name='código de activación')
    reset_code = models.CharField(max_length=255, null=True, blank=True,
                                  verbose_name='código de reseteo')

    def questions_count(self):
        from estimations.models import Question
        return Question.objects.filter(user=self).count()
    questions_count.short_description = 'cantidad preguntas'

    def quotation_count(self):
        return self.quotations.all().count()
    quotation_count.short_description = 'cantidad cotizaciones'

    def purchase_count(self):
        return self.purchases.all().count()
    purchase_count.short_description = 'cantidad compras'

    def sale_count(self):
        return self.sales.all().count()
    sale_count.short_description = 'cantidad ventas'

    def offer_count(self):
        return self.offers.all().count()
    offer_count.short_description = 'cantidad ofertas'

    def seller_reputation(self):
        value = 0
        count = 0
        for sale in self.sales.all():
            for calification in sale.seller_califications.all():
                value += calification.calification
                count += 1

        if value:
            value = value / count

        return value

    def seller_comments(self):
        comments = []
        for sale in self.sales.all():
            for calification in sale.seller_califications.all():
                comments.append({
                    'date': calification.calification_date,
                    'user': calification.purchase.buyer.username,
                    'comment': calification.comments
                })

        return comments

    def buyer_reputation(self):
        value = 0
        count = 0
        for purchase in self.purchases.all():
            for calification in purchase.buyer_califications.all():
                value += calification.calification
                count += 1

        if value:
            value = value / count

        return value

    def buyer_comments(self):
        comments = []
        for purchase in self.purchases.all():
            for calification in purchase.buyer_califications.all():
                comments.append({
                    'date': calification.calification_date,
                    'user': calification.purchase.seller.username,
                    'comment': calification.comments
                })

        return comments

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    def __unicode__(self):
        return str(self.username)
