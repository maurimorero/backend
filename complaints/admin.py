from django.contrib import admin

from rangefilter.filter import DateRangeFilter

from complaints.models import *


class ComplaintAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'motive', 'state', 'purchase', 'id_purchase',
                    'buyer', 'seller')
    list_filter = ('state', ('date', DateRangeFilter))
    search_fields = [
                    'purchase__seller__username', 'purchase__buyer__username',
                    'purchase__seller__first_name',
                    'purchase__seller__last_name',
                    'purchase__buyer__first_name',
                    'purchase__buyer__last_name',
                    'motive'
    ]


admin.site.register(Complaint, ComplaintAdmin)
admin.site.register(Question)
