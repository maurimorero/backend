from __future__ import unicode_literals

from django.db import models
from users.models import AuctionUser
from purchases.models import Purchase


class Complaint(models.Model):
    IN_PROGRESS = 'I'
    FINISHED = 'F'

    date = models.DateField(auto_now_add=True, verbose_name='fecha')
    state = models.CharField(max_length=1, choices=(
            (IN_PROGRESS, 'En progreso'),
            (FINISHED, 'Finalizado')
        ),
        default='I', verbose_name='estado'
    )
    complaint_type = models.CharField(max_length=1, choices=(
            ('D', 'Es distinto a lo que compre'),
            ('B', 'Llego roto'),
            ('L', 'Nunca llego'),
            ('O', 'Otro')
        ),
        null=True, blank=True, verbose_name='tipo de queja'
    )
    motive = models.TextField(max_length=255, verbose_name='motivo')
    purchase = models.ForeignKey(Purchase, null=True, blank=False,
                                 related_name='complaints',
                                 verbose_name='compra')

    def seller(self):
        return self.purchase.seller.username if self.purchase else ''
    seller.short_description = 'vendedor'

    def buyer(self):
        return self.purchase.buyer.username if self.purchase else ''
    buyer.short_description = 'comprador'

    def id_purchase(self):
        return self.purchase.id if self.purchase else ''
    id_purchase.short_description = 'ID compra'

    class Meta:
        verbose_name = "Queja"
        verbose_name_plural = "Quejas"

    def __unicode__(self):
        return self.motive


class Question(models.Model):
    date_created = models.DateField(auto_now_add=True, verbose_name='fecha')
    user = models.ForeignKey(AuctionUser, verbose_name='usuario',
                             related_name='complaint_questions')
    complaint = models.ForeignKey(
        Complaint, null=True, blank=True, related_name='questions',
        verbose_name='queja'
    )
    question = models.CharField(max_length=255, verbose_name='pregunta')

    class Meta:
        verbose_name = 'Pregunta'
        verbose_name_plural = 'Preguntas'

    def __unicode__(self):
        return self.question


class Reply(models.Model):
    date_created = models.DateField(auto_now_add=True)
    user = models.ForeignKey(AuctionUser, related_name='complaint_replies')
    answer = models.CharField(max_length=255)
    question = models.ForeignKey(Question, related_name='complaint_replies')

    def __unicode__(self):
        return self.answer
