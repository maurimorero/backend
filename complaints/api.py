# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.forms.models import model_to_dict

from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication
from tastypie.exceptions import BadRequest

from complaints.models import *
from purchases.models import Purchase, PurchaseState


class ComplaintResource(ModelResource):
    complaint_type = fields.CharField()
    state = fields.CharField(attribute="state")
    product = fields.CharField(attribute="product", null=True)

    def dehydrate_complaint_type(self, bundle):
        return bundle.obj.get_complaint_type_display()

    def dehydrate_state(self, bundle):
        return bundle.obj.get_state_display()

    def dehydrate_product(self, bundle):
        if bundle.obj.purchase.product:
            return bundle.obj.purchase.product.name
        elif bundle.obj.purchase.offer:
            offer = bundle.obj.purchase.offer
            if offer.estimation:
                return offer.estimation.product.name
            elif offer.custom_estimation:
                return offer.custom_estimation.requested_product
        return ""

    class Meta:
        queryset = Complaint.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'complaint'
        authentication = SessionAuthentication()
        filtering = {
            'state': ALL
        }
        ordering = ['date']

    def dehydrate(self, bundle):
        product = None

        if bundle.obj.purchase.product:
            product = bundle.obj.purchase.product
        elif bundle.obj.purchase.offer:
            offer = bundle.obj.purchase.offer
            if offer.estimation:
                product = offer.estimation.product

        if product:
            bundle.data['image_1'] = product.get_thumb_1()
            bundle.data['image_2'] = product.get_thumb_2()
            bundle.data['image_3'] = product.get_thumb_3()
            bundle.data['image_4'] = product.get_thumb_4()
            bundle.data['image_5'] = product.get_thumb_5()
            bundle.data['image_6'] = product.get_thumb_6()

        return bundle

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/create-complaint%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_complaint'),
                name="api_create_complaint"
            ),
            url(
                r"^(?P<resource_name>%s)/complaints-buyer%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('complaints_buyer'),
                name="api_complaints_buyer"
            ),
            url(
                r"^(?P<resource_name>%s)/complaints-seller%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('complaints_seller'),
                name="api_complaints_seller"
            ),
        ]

    def create_complaint(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        purchase_id = data.get('purchase_id')
        motive = data.get('motive', '')
        complaint_type = data.get('complaint_type', '')

        purchase = Purchase.objects.get(id=purchase_id)
        state = purchase.state
        state.state = PurchaseState.CONFLICT
        state.save()

        complaint = Complaint()
        complaint.purchase = purchase
        complaint.complaint_type = complaint_type
        complaint.motive = motive

        complaint.save()

        return self.create_response(request, {})

    def complaints_buyer(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        result = []

        complaints = Complaint.objects.filter(purchase__buyer=request.user)

        if 'state' in request.GET and request.GET['state'] == 'I':
            complaints = complaints.filter(state='I')

        if 'state' in request.GET and request.GET['state'] == 'F':
            complaints = complaints.filter(state='F')

        if 'sort_by' in request.GET and request.GET['sort_by'] == 'date':
            complaints = complaints.order_by("date")

        for c in complaints:
            data = model_to_dict(c)
            if c.purchase.product:
                data["product"] = c.purchase.product.name

            elif c.purchase.offer.estimation:
                data["product"] = c.purchase.offer.estimation.product.name

            elif c.purchase.offer.custom_estimation:
                data["product"] = c.purchase.offer.custom_estimation.requested_product

            data["count_msgs"] = c.questions.all().count()
            data["complaint_type"] = c.get_complaint_type_display()
            data["state"] = c.get_state_display()

            data['image_1'] = c.purchase.product.image_1
            data['image_2'] = c.purchase.product.image_2
            data['image_3'] = c.purchase.product.image_3
            data['image_4'] = c.purchase.product.image_4
            data['image_5'] = c.purchase.product.image_5
            data['image_6'] = c.purchase.product.image_6

            result.append(data)

        return self.create_response(request, result)

    def complaints_seller(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        result = []

        complaints = Complaint.objects.filter(purchase__seller=request.user)

        if 'state' in request.GET and request.GET['state'] == 'I':
            complaints = complaints.filter(state='I')

        if 'state' in request.GET and request.GET['state'] == 'F':
            complaints = complaints.filter(state='F')

        if 'sort_by' in request.GET and request.GET['sort_by'] == 'date':
            complaints = complaints.order_by("date")

        if 'sort_by' in request.GET and request.GET['sort_by'] == '-date':
            complaints = complaints.order_by("-date")

        for c in complaints:
            data = model_to_dict(c)
            if c.purchase.product:
                data["product"] = c.purchase.product.name

            elif c.purchase.offer.estimation:
                data["product"] = c.purchase.offer.estimation.product.name

            elif c.purchase.offer.custom_estimation:
                data["product"] = c.purchase.offer.custom_estimation.requested_product

            data["count_msgs"] = c.questions.all().count()
            data["state"] = c.get_state_display()
            data["date"] = c.date
            data["complaint_type"] = c.get_complaint_type_display()

            data['image_1'] = c.purchase.product.image_1
            data['image_2'] = c.purchase.product.image_2
            data['image_3'] = c.purchase.product.image_3
            data['image_4'] = c.purchase.product.image_4
            data['image_5'] = c.purchase.product.image_5
            data['image_6'] = c.purchase.product.image_6

            result.append(data)

        return self.create_response(request, result)


class ComplaintQuestionResource(ModelResource):
    complaint_id = fields.IntegerField(attribute='complaint__id', null=True)
    user = fields.CharField(attribute='user')

    def dehydrate(self, bundle):
        # Add the responses of the answer
        replies = []
        for reply in bundle.obj.complaint_replies.all():
            replies.append(
                {
                    'id': reply.id,
                    'user': reply.user,
                    'date_created': reply.date_created,
                    'answer': reply.answer
                }
            )

        bundle.data['replies'] = replies
        return bundle

    class Meta:
        queryset = Question.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'complaint-question'
        # authentication = SessionAuthentication()
        filtering = {
            'complaint_id': ALL,
            'user': ALL,
        }

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/create-question%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_question'),
                name="api_create_question"
            ),
            url(
                r"^(?P<resource_name>%s)/create-reply%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_reply'),
                name="api_create_reply"
            )
        ]

    def create_question(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        complaint_id = data.get('complaint_id', '')

        question = Question()

        question.complaint = Complaint.objects.get(id=complaint_id)
        question.user = request.user
        question.question = data.get('question')
        question.save()

        return self.create_response(request, {})

    def create_reply(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        question_id = data.get('question_id')

        reply = Reply()
        reply.question = Question.objects.get(id=question_id)
        reply.user = request.user
        reply.answer = data.get('answer')
        reply.save()

        return self.create_response(request, {})
