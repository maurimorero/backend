define({
  "name": "Auctions API",
  "version": "0.0.1",
  "description": "API documentation for Auction project",
  "title": "API documentation for Auction project",
  "url": "http://localhost:8000/api/v1",
  "template": {
    "withCompare": true,
    "withGenerator": true
  },
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-08-06T21:44:46.579Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
