define({ "api": [
  {
    "type": "get",
    "url": "/activitylog/user-action-logs/",
    "title": "User activity Log",
    "version": "0.0.1",
    "name": "ActivityLog",
    "group": "ActivityLog",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "last_login",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "last_purchase",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "purchase_count",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "last_sale",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sales_count",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "ActivityLog"
  },
  {
    "type": "post",
    "url": "/complaint/create-complaint/",
    "title": "Create complaint",
    "version": "0.0.1",
    "name": "complaintCreate",
    "group": "Complaint",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "purchase_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "motive",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "complaint_type",
            "description": "<p>D (Es distinto a lo que compre) B (Llego roto) L (Nunca llego) O (Otro)</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"motive\": \"alguno\", \"purchase_id\": 5, \"complaint_type\": \"D\"}' \n http://localhost:8000/api/v1/complaint/create-complaint/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Complaint"
  },
  {
    "type": "post",
    "url": "/custom-estimation/create-custom-estimation/",
    "title": "Custom Estimation create",
    "version": "0.0.1",
    "name": "CustomEstimationCreate",
    "group": "CustomEstimation",
    "description": "<p>The format date MUST be YYYY-MM-DD</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "requested_product",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "count",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "buyer_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comments",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "category_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "sub_category_id",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n{\"date_end\":\"2016-10-28\", \"count\":1, \"buyer_id\":1,\"comments\": \"some comment\", \n\"requested_product\": \"Un prodcuto custom\", \"category_id\": 1, \"sub_category_id\": 1}\nhttp://localhost:8000/api/v1/custom-estimation/create-custom-estimation/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/custom-estimation-create-offer/",
    "title": "Create offer for custom estimation",
    "version": "0.0.1",
    "name": "CustomEstimationCreateOffer",
    "group": "CustomEstimation",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "seller_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "estmation_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"seller_id\": 1, \"estimation_id\": 1, \"price\": \"2.45\"}'\nhttp://localhost:8000/api/v1/custom-estimation/custom-estimation-create-offer/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/",
    "title": "Custom Estimation list",
    "version": "0.0.1",
    "name": "CustomEstimationList",
    "group": "CustomEstimation",
    "description": "<p>Returns all custom estimations</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n    \"buyer\": \"admin\",\n    \"buyer_id\": 1,\n    \"category\": \"Celulares\",\n    \"category_id\": 1,\n    \"comments\": \"some comment\",\n    \"count\": 1,\n    \"date_created\": \"2016-12-15\",\n    \"date_end\": \"2016-10-28\",\n    \"id\": 1,\n    \"is_active\": false,\n    \"offers\": [\n      {\n        \"id\": 2,\n        \"location\": \"\",\n        \"price\": \"2123123.00\",\n        \"seller\": \"Alejandro Dev\",\n        \"seller_id\": 1\n      }\n    ],\n    \"product_description\": null,\n    \"requested_product\": \"Algun producto custom\",\n    \"sub_category\": \"Tegnologia\",\n    \"sub_category_id\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?active",
    "title": "CustomEstimation list filter by active",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterByActive",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?cateogry=ID",
    "title": "Filter by Category",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterByCategory",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?buyer__id=ID",
    "title": "Only my custom estimations",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterByOnlyMyselfEstimations",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?sub_cateogry=ID",
    "title": "Filter by SubCategory",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterBySubCategory",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?canceled=True",
    "title": "CustomEstimation list filter canceled",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterCanceled",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?expired=True|False",
    "title": "CustomEstimation list filter expired",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterExpired",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?successful=True",
    "title": "CustomEstimation list filter successful",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterSuccessful",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?with-offers=True",
    "title": "CustomEstimation list filter with offers",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterWithOffers",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?with-offers=False",
    "title": "CustomEstimation list filter without offers",
    "version": "0.0.1",
    "name": "CustomEstimationListFilterWithoutOffers",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?order_by=date_created",
    "title": "CustomEstimation list order by date created",
    "version": "0.0.1",
    "description": "<p>For descending order use -date_created</p>",
    "name": "CustomEstimationListOrderByDateCreated",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?order_by=date_end",
    "title": "CustomEstimation list order by date end",
    "version": "0.0.1",
    "description": "<p>For descending order use -date_end</p>",
    "name": "CustomEstimationListOrderByDateEnd",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimation/?buyer__id=ID&active=True|False",
    "title": "User active or Inactive CustomEstimation",
    "version": "0.0.1",
    "name": "EstimationListFilterByActiveOrInactiveEstimations",
    "group": "CustomEstimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/custom-estimations/get-estimation-attributes/ID/",
    "title": "List custom estimations attributes",
    "version": "0.0.1",
    "name": "ListCustomEstimationAttributtes",
    "group": "CustomEstimation",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\"group\": 1, \"group_name\": \"Informacion del producto\", \"id\": 1, \"name\": \"Pantalla\", \"sub_category\": 1}, \n{\"group\": 1, \"group_name\": \"Informacion del producto\", \"id\": 2, \"name\": \"Sistema operativo\", \"sub_category\": 1}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "post",
    "url": "/custom-estimation/set-estimation-attributes/ID/",
    "title": "Set product attributes",
    "version": "0.0.1",
    "name": "SetCustomEstimationAttributes",
    "group": "CustomEstimation",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"attrs\": [{\"value\": \"Android\", \"attribute_id\": 2}]}' \nhttp://localhost:8000/api/v1/product/set-product-attributes/PRODUCTO-ID/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "CustomEstimation"
  },
  {
    "type": "get",
    "url": "/estimation/create-generic-estimation/",
    "title": "Estimation create",
    "version": "0.0.1",
    "name": "EstimationCreate",
    "group": "Estimation",
    "description": "<p>The format date MUST be YYYY-MM-DD</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "product_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "count",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "buyer_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comments",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"product_id\":1, \"date_end\":\"2016-10-28\", \"count\":1, \"buyer_id\":1,\n\"comments\": \"some comment\"}' \nhttp://localhost:8000/api/v1/estimation/create-generic-estimation/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/estimation-create-offer/",
    "title": "Create offer for estimation",
    "version": "0.0.1",
    "name": "EstimationCreateOffer",
    "group": "Estimation",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "seller_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "estmation_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"seller_id\": 1, \"estimation_id\": 1, \"price\": \"2.45\"}'\nhttp://localhost:8000/api/v1/estimation/estimation-create-offer/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/ID/",
    "title": "Estimation detail",
    "version": "0.0.1",
    "name": "EstimationDetail",
    "group": "Estimation",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t\t\"is_active\": true, \n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"date_end\": \"2016-10-28\", \n\t\t\"id\": 1, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"comments\": \"\",\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/",
    "title": "Estimation list",
    "version": "0.0.1",
    "name": "EstimationList",
    "group": "Estimation",
    "description": "<p>Returns all estimations</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t\t\"is_active\": false,\n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"date_end\": \"2016-10-28\", \n\t\t\"id\": 1, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"comments\": \"\",\n     \"offers\": [{\"id\": 1, \"location\": \"\", \"price\": \"123123.00\", \"seller\": \"Alejandro Dev\", \"seller_id\": 1}]\n\t}, \n\t{\n\t\t\"is_active\": true, \n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"date_end\": \"2016-10-28\", \n\t\t\"id\": 2, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"comments\": \"\",\n     \"offers\": [{\"id\": 1, \"location\": \"\", \"price\": \"123123.00\", \"seller\": \"Alejandro Dev\", \"seller_id\": 1}]\n\t}, \n\t{\n\t\t\"active\": true, \n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"date_end\": \"2016-10-28\", \n\t\t\"id\": 3, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"comments\": \"\",\n     \"offers\": [{\"id\": 1, \"location\": \"\", \"price\": \"123123.00\", \"seller\": \"Alejandro Dev\", \"seller_id\": 1}]\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?active",
    "title": "Estimation list filter by active",
    "version": "0.0.1",
    "name": "EstimationListFilterByActive",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?buyer__id=ID&active=True|False",
    "title": "User active or Inactive Estimations",
    "version": "0.0.1",
    "name": "EstimationListFilterByActiveOrInactiveEstimations",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?cateogry=ID",
    "title": "Filter by Category",
    "version": "0.0.1",
    "name": "EstimationListFilterByCategory",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?buyer__id=ID",
    "title": "Only my estimations",
    "version": "0.0.1",
    "name": "EstimationListFilterByOnlyMyselfEstimations",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?sub_cateogry=ID",
    "title": "Filter by SubCategory",
    "version": "0.0.1",
    "name": "EstimationListFilterBySubCategory",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?canceled=True",
    "title": "Estimation list filter canceled",
    "version": "0.0.1",
    "name": "EstimationListFilterCanceled",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?expired=True|False",
    "title": "Estimation list filter expired",
    "version": "0.0.1",
    "name": "EstimationListFilterExpired",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?successful=True",
    "title": "Estimation list filter successful",
    "version": "0.0.1",
    "name": "EstimationListFilterSuccessful",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?with-offers=True",
    "title": "Estimation list filter with offers",
    "version": "0.0.1",
    "name": "EstimationListFilterWithOffers",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?with-offers=False",
    "title": "Estimation list filter without offers",
    "version": "0.0.1",
    "name": "EstimationListFilterWithoutOffers",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?order_by=date_created",
    "title": "Estimation list order by date created",
    "version": "0.0.1",
    "description": "<p>For descending order use -date_created</p>",
    "name": "EstimationListOrderByDateCreated",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/estimation/?order_by=date_end",
    "title": "Estimation list order by date end",
    "version": "0.0.1",
    "description": "<p>For descending order use -date_end</p>",
    "name": "EstimationListOrderByDateEnd",
    "group": "Estimation",
    "filename": "backend/apidoc.js",
    "groupTitle": "Estimation"
  },
  {
    "type": "get",
    "url": "/offer/",
    "title": "My offers",
    "version": "0.0.1",
    "name": "OfferList",
    "group": "Offer",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n    \"category\": \"Tegnologia\",\n    \"category_id\": 1,\n    \"custom_estimation\": {\n      \"buyer\": 4,\n      \"category\": 1,\n      \"comments\": \"sssddd\",\n      \"count\": 2,\n      \"date_end\": \"2016-12-22\",\n      \"id\": 1,\n      \"requested_product\": \"asbiii\",\n      \"sub_category\": 1\n    },\n    \"date_created\": \"2016-12-07\",\n    \"estimation\": {\n      \"buyer\": 1,\n      \"comments\": \"Comentarios\",\n      \"count\": 3,\n      \"date_end\": \"2016-12-22\",\n      \"id\": 5,\n      \"product\": 2\n    },\n    \"id\": 3,\n    \"price\": \"6000.00\",\n    \"sub_category\": \"Celulares\",\n    \"sub_category_id\": 1\n  },\n  {\n    \"category\": \"Tegnologia\",\n    \"category_id\": 1,\n    \"custom_estimation\": \"\",\n    \"date_created\": \"2017-02-01\",\n    \"estimation\": {\n      \"buyer\": 2,\n      \"comments\": \"Something\",\n      \"count\": 5,\n      \"date_end\": \"2016-12-11\",\n      \"id\": 1,\n      \"product\": 1\n    },\n    \"id\": 4,\n    \"price\": \"2.45\",\n    \"sub_category\": \"Celulares\",\n    \"sub_category_id\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Offer"
  },
  {
    "type": "get",
    "url": "/category/",
    "title": "Category list",
    "version": "0.0.1",
    "name": "CategoryList",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"Tecnologia\"\n},\n{\n  \"id\": 2,\n  \"name\": \"Electrodomesticos\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/category/?name__contains=CategoryName",
    "title": "Category list search/filter",
    "version": "0.0.1",
    "name": "CategoryListFilter",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"Tecnologia\"\n},\n{\n  \"id\": 2,\n  \"name\": \"Electrodomesticos\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Accept: application/json\" http://localhost:8000/api/v1/category/?name__contains=Electrodomesticos",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/product/?user=ID",
    "title": "Get products loaded by the user",
    "version": "0.0.1",
    "name": "GetProductsLoaded",
    "group": "Product",
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/product/get-favorite-products/",
    "title": "Get user favorite products",
    "version": "0.0.1",
    "name": "GetUserFavoriteProducts",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"category\": \"Tegnologia\", \n       \"category_id\": 1, \n       \"description\": \"\", \n       \"id\": 1, \n       \"image_1\": \"static/image/5132014122216PM_635_motorola_moto_e.jpeg\", \n       \"image_2\": \"\", \n       \"image_3\": \"\", \n       \"image_4\": \"\", \n       \"image_5\": \"\", \n       \"image_6\": \"\", \n       \"name\": \"Moto E\", \n       \"state\": \"U\", \n       \"sub_category\": \"Celulares\", \n       \"sub_category_id\": 1, \n       \"thumb_url_1\": \"static/image/5132014122216PM_635_motorola_moto_e.jpeg.340x200_q85_crop.jpg\", \n       \"thumb_url_2\": \"\", \n       \"thumb_url_3\": \"\", \n       \"thumb_url_4\": \"\", \n       \"thumb_url_5\": \"\", \n       \"thumb_url_6\": \"\"\n  },\n{\n       \"category\": \"Tegnologia\", \n       \"category_id\": 1, \n       \"description\": \"\", \n       \"id\": 1, \n       \"image_1\": \"static/image/5132014122216PM_635_motorola_moto_e.jpeg\", \n       \"image_2\": \"\", \n       \"image_3\": \"\", \n       \"image_4\": \"\", \n       \"image_5\": \"\", \n       \"image_6\": \"\", \n       \"name\": \"Moto E\", \n       \"state\": \"U\", \n       \"sub_category\": \"Celulares\", \n       \"sub_category_id\": 1, \n       \"thumb_url_1\": \"static/image/5132014122216PM_635_motorola_moto_e.jpeg.340x200_q85_crop.jpg\", \n       \"thumb_url_2\": \"\", \n       \"thumb_url_3\": \"\", \n       \"thumb_url_4\": \"\", \n       \"thumb_url_5\": \"\", \n       \"thumb_url_6\": \"\"\n  },",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/product/get-product-attributes/PRODUCT-ID/",
    "title": "List product attributes",
    "version": "0.0.1",
    "name": "ListProductAttributtes",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\"group\": 1, \"group_name\": \"Informacion del producto\", \"id\": 1, \"name\": \"Pantalla\", \"sub_category\": 1}, \n{\"group\": 1, \"group_name\": \"Informacion del producto\", \"id\": 2, \"name\": \"Sistema operativo\", \"sub_category\": 1}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/product/load-product/",
    "title": "Load product",
    "version": "0.0.1",
    "name": "LoadProduct",
    "group": "Product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "category_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "sub_category_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "name",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "description",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "state",
            "description": "<p>N for new, or U for used</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Base64",
            "optional": false,
            "field": "image_1",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "Base64",
            "optional": false,
            "field": "image_2",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "Base64",
            "optional": false,
            "field": "image_3",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "Base64",
            "optional": false,
            "field": "image_4",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "Base64",
            "optional": false,
            "field": "image_5",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "Base64",
            "optional": false,
            "field": "image_6",
            "description": "<p>Optional</p>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/product/ID/",
    "title": "Product detail",
    "version": "0.0.1",
    "name": "ProductDetail",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t\t\t\"category\": \"Tegnologia\",\n\t\t\t\"category_id\": 1, \n\t\t\t\"id\": 1, \n\t\t\t\"name\": \"Nokia\", \n\t\t\t\"prices\": [{\"id\": 1, \"seller_name\": \"Cosme Fulanito\"}], \n\t\t\t\"sub_category\": \"Celulares\", \n\t\t\t\"sub_category_id\": 1,\n\t\t\t\"attributes\": [{\"name\": \"Sistema Operativo\", \"value\": \"Android\"}]\n\t\t}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/product/?category_id=ID&sub_category_id=ID&name__contains=Something",
    "title": "Product list",
    "version": "0.0.1",
    "name": "ProductList",
    "group": "Product",
    "description": "<p>If call the endpoint without parameters (name, category_id, or sub_category_id), then returns all products.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t\t\t\"category\": \"Tegnologia\",\n\t\t\t\"category_id\": 1, \n\t\t\t\"id\": 1, \n\t\t\t\"name\": \"Nokia\", \n\t\t\t\"prices\": [{\"id\": 1, \"seller_name\": \"Cosme Fulanito\"}], \n\t\t\t\"sub_category\": \"Celulares\", \n\t\t\t\"sub_category_id\": 1\n\t\t}, \n\t\t{\n\t\t\t\"category\": \"Tegnologia\", \n\t\t\t\"category_id\": 1, \n\t\t\t\"id\": 2, \n\t\t\t\"name\": \"Iphone\", \n\t\t\t\"prices\": [], \n\t\t\t\"sub_category\": \"Celulares\", \n\t\t\t\"sub_category_id\": 1\n\t\t}, \n\t\t{\n\t\t\t\"category\": \"Electrodomesticos\", \n\t\t\t\"category_id\": 2, \n\t\t\t\"id\": 3, \n\t\t\t\"name\": \"Samsung 20 pulgadas\", \n\t\t\t\"prices\": [], \n\t\t\t\"sub_category\": \"Televisores\", \n\t\t\t\"sub_category_id\": 2\n\t\t}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Accept: application/json\" \nhttp://localhost:8000/api/v1/product/?category_id=2&sub_category_id=2&name__contains=Samsung\n\ncurl -H \"Accept: application/json\" \nhttp://localhost:8000/api/v1/product/?category_id=1&sub_category_id=1\n\n\ncurl -H \"Accept: application/json\" \nhttp://localhost:8000/api/v1/product/?category_id=1\n\n\ncurl -H \"Accept: application/json\" \nhttp://localhost:8000/api/v1/product/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/product/?user_id=USER_ID/",
    "title": "Product filter by user preferences",
    "version": "0.0.1",
    "name": "ProductListByUserPreferences",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t\t\t\"category\": \"Tegnologia\",\n\t\t\t\"category_id\": 1, \n\t\t\t\"id\": 1, \n\t\t\t\"name\": \"Nokia\", \n\t\t\t\"prices\": [{\"id\": 1, \"seller_name\": \"Cosme Fulanito\"}], \n\t\t\t\"sub_category\": \"Celulares\", \n\t\t\t\"sub_category_id\": 1\n\t\t}, \n\t\t{\n\t\t\t\"category\": \"Tegnologia\", \n\t\t\t\"category_id\": 1, \n\t\t\t\"id\": 2, \n\t\t\t\"name\": \"Iphone\", \n\t\t\t\"prices\": [], \n\t\t\t\"sub_category\": \"Celulares\", \n\t\t\t\"sub_category_id\": 1\n\t\t}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/product/set-price/",
    "title": "Set price",
    "version": "0.0.1",
    "name": "SetPrice",
    "group": "Product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "seller_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "product_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"seller_id\": 1, \"price\": \"2.80\", \"product_id\": 1}'\nhttp://localhost:8000/api/v1/product/set-price/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/product/set-price/",
    "title": "Set price",
    "version": "0.0.1",
    "name": "SetPrice",
    "group": "Product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "seller_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "product_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"seller_id\": 1, \"price\": \"2.80\", \"product_id\": 1}'\nhttp://localhost:8000/api/v1/product/set-price/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/product/set-product-as-favorite/",
    "title": "Set product as favorite",
    "version": "0.0.1",
    "name": "SetProductAsFavorite",
    "group": "Product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "product_id",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/product/set-product-attributes/PRODUCT-ID/",
    "title": "Set product attributes",
    "version": "0.0.1",
    "name": "SetProductAttributes",
    "group": "Product",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"attrs\": [{\"value\": \"Android\", \"attribute_id\": 2}]}' \nhttp://localhost:8000/api/v1/product/set-product-attributes/PRODUCTO-ID/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/subcategory/",
    "title": "SubCategory list",
    "version": "0.0.1",
    "name": "SubCategoryList",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"Celulares\",\n  \"category\": \"Tegnologia\",\n  \"category_id\": 1,\n},\n{\n  \"id\": 2,\n  \"name\": \"Televisores\",\n  \"category\": \"Tegnologia\",\n  \"category_id\": 1,\n}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/subcategory/?category_id=ID",
    "title": "SubCategory list filter by category",
    "version": "0.0.1",
    "name": "SubCategoryListFilterByCategory",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"Celulares\"\n  \"category\": \"Tegnologia\",\n  \"category_id\": 1,\n},\n{\n  \"id\": 2,\n  \"name\": \"Televisores\",\n  \"category\": \"Tegnologia\",\n  \"category_id\": 1,\n}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/subcategory/?name__contains=SubCategoryName",
    "title": "SubCategory list search/filter by name",
    "version": "0.0.1",
    "name": "SubCategoryListFilterByName",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"Celulares\"\n  \"category\": \"Tegnologia\",\n  \"category_id\": 1,\n},\n{\n  \"id\": 2,\n  \"name\": \"Televisores\",\n  \"category\": \"Tegnologia\",\n  \"category_id\": 1,\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Accept: application/json\" http://localhost:8000/api/v1/subcategory/?name__contains=Celulares",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/product/get-user-products/",
    "title": "Get user products",
    "version": "0.0.1",
    "name": "UserProductList",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n [{\n     \"category\": Tegnologia, \n     \"category_id\": 1,\n     \"sub_category\": \"Tegnologia\", \n     \"sub_category_id\": 1\n     \"description\": \"Moto E segunda generaciÃ³n\", \n     \"id\": 1, \n     \"image\": \"\", \n     \"name\": \"Moto E\", \n     \"price\": \"5.00\", \n     \"state\": null, \n     \"sub_category\": 1\n }]",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/purchase/buyer-calification/",
    "title": "Buyer calification",
    "version": "0.0.1",
    "name": "BuyerCalification",
    "group": "Purchase",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "calification",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "purchase_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comments",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"calification\": 4, \"purchase_id\": \"1\"}'\nhttp://localhost:8000/api/v1/purchase/buyer-calification/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "get",
    "url": "/purchase/create-purchase/",
    "title": "Purchase create",
    "version": "0.0.1",
    "name": "PurchaseCreate",
    "group": "Purchase",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "product_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "count",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "seller_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "buyer_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "price_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "offer_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "delivery",
            "description": "<p>P (Pick up at seller address) or D (Door to door service / delivery)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "street_number",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "floor",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "unit",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "area_code",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"product_id\":1, \"count\": 1, \"seller_id\": 1, \"delivery\": \"P\", \"buyer_id\": 1}'\nhttp://localhost:8000/api/v1/purchase/create-purchase/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "get",
    "url": "/purchase/ID/",
    "title": "Purchase detail",
    "version": "0.0.1",
    "name": "PurchaseDetail",
    "group": "Purchase",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"id\": 1, \n\t\t\"price\": \"1500.00\",\n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"delivery\": \"Pick up at seller address\",\n\t\t\"seller_data\": {\"email\": \"\", \"name\": \"Alejandro Dev\", \"phone\": null}\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "get",
    "url": "/purchase/?buyer_id=ID",
    "title": "Purchase list",
    "version": "0.0.1",
    "name": "PurchaseList",
    "group": "Purchase",
    "description": "<p>Returns all purchases</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n    {\n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"id\": 1, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"delivery\": \"Pick up at seller address\"\",\n\t},\n    {\n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"id\": 1, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"delivery\": \"Door to door service / delivery\",\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "post",
    "url": "/purchase/set-state-to-sent/",
    "title": "Purchase set state to sent",
    "version": "0.0.1",
    "name": "PurchaseSetStateToSent",
    "group": "Purchase",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "purchase_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "type_sent_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tracking_code",
            "description": "<p>optional</p>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "get",
    "url": "/purchase/?state=P|S|F|C|W|R",
    "title": "Purchase state filters",
    "version": "0.0.1",
    "name": "PurchaseStateFilters",
    "group": "Purchase",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "P",
            "description": "<p>In Progress</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "S",
            "description": "<p>Sent</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "F",
            "description": "<p>Finished</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "C",
            "description": "<p>Canceled</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "W",
            "description": "<p>Conflict</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "R",
            "description": "<p>Received</p>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "get",
    "url": "/purchase/?seller_id=ID",
    "title": "Sales list",
    "version": "0.0.1",
    "name": "SalesList",
    "group": "Purchase",
    "description": "<p>Returns all sales</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n    {\n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"id\": 1, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"delivery\": \"Pick up at seller address\"\",\n\t},\n    {\n\t\t\"buyer\": \"dev\",\n\t\t\"buyer_id\": 1,\n\t\t\"category\": \"Tegnologia\", \n\t\t\"category_id\": 1, \n\t\t\"count\": 1, \n\t\t\"date_created\": \"2016-10-28\", \n\t\t\"id\": 1, \n\t\t\"product\": \"Nokia\", \n\t\t\"product_id\": 1, \n\t\t\"sub_category\": \"Celulares\", \n\t\t\"sub_category_id\": 1,\n\t\t\"delivery\": \"Door to door service / delivery\",\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "post",
    "url": "/purchase/seller-calification/",
    "title": "Seller calification",
    "version": "0.0.1",
    "name": "SellerCalification",
    "group": "Purchase",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "calification",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "purchase_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comments",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"calification\": 4, \"purchase_id\": \"1\"}'\nhttp://localhost:8000/api/v1/purchase/seller-calification/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Purchase"
  },
  {
    "type": "post",
    "url": "/question/create-question/",
    "title": "Create Question",
    "version": "0.0.1",
    "name": "QuestionCreate",
    "group": "Question",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "estimation_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "question",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"estimation_id\": 1, \"question\": \"este producto apesta?\"}'\nhttp://localhost:8000/api/v1/question/create-question/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Question"
  },
  {
    "type": "get",
    "url": "/question//?user=USER_ID",
    "title": "Questions created",
    "version": "0.0.1",
    "name": "QuestionCreated",
    "group": "Question",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n{\n    \"date_created\": \"2017-05-15\", \n    \"estimation_id\": 1, \n    \"id\": 1, \n    \"question\": \"Some question\", \n    \"replies\": [], \n    \"user\": \"dev\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Question"
  },
  {
    "type": "get",
    "url": "/question/?estimation_id=ESTIMATION_ID/",
    "title": "Get question list of a estimation",
    "version": "0.0.1",
    "name": "QuestionList",
    "group": "Question",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n\n {\n \"date_created\": \"2016-12-12\",\n  \"estimation_id\": 1,\n  \"id\": 1,\n  \"question\": \"Esta que mola este producto?\",\n  \"replies\": [\n    {\n      \"answer\": \"Yes, es un super producto\",\n      \"date_created\": \"2016-12-12\",\n      \"id\": 1,\n      \"user\": \"admin\"\n    }\n  ],\n  \"user\": \"Alejandro\"\n},\n{\n  \"date_created\": \"2016-12-12\",\n  \"estimation_id\": 1,\n  \"id\": 2,\n  \"question\": \"este producto apesta?\",\n  \"replies\": [\n    {\n      \"answer\": \"Si la verdad que este producto apesta\",\n      \"date_created\": \"2016-12-12\",\n      \"id\": 2,\n      \"user\": \"admin\"\n    }\n  ],\n  \"user\": \"Alejandro\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Question"
  },
  {
    "type": "get",
    "url": "/question/questions-received/",
    "title": "Questions received",
    "version": "0.0.1",
    "name": "QuestionsReceived",
    "group": "Question",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n{\"estimation\": 1, \"id\": 1, \"question\": \"Some question\", \"user\": 1}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "Question"
  },
  {
    "type": "post",
    "url": "/question/create-reply/",
    "title": "Create Reply",
    "version": "0.0.1",
    "name": "ReplyCreate",
    "group": "Question",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "question_id",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "answer",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"question_id\": 2, \"answer\": \"este producto es una masa\"}'\nhttp://localhost:8000/api/v1/api/v1/question/create-reply/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "Question"
  },
  {
    "type": "get",
    "url": "/type-sent/",
    "title": "Type Sent list",
    "version": "0.0.1",
    "name": "TypeSentList",
    "group": "TypeSent",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n  \"id\": 1, \n  \"name\": \"Correo Argentino\"\n },\n {\n  \"id\": 2, \n  \"name\": \"DHL\"\n },",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "TypeSent"
  },
  {
    "type": "post",
    "url": "/user/activate-user/",
    "title": "Activate user",
    "version": "0.0.1",
    "name": "ActivateUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/change-password/",
    "title": "Change password",
    "version": "0.0.1",
    "name": "ChangePassword",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/signup/",
    "title": "Create user",
    "version": "0.0.1",
    "name": "CreateUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>(optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>(optional)</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Response (Error response example):",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"error\": \"Nombre de usuario no disponible\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t\t\"area_code\": null,\n\t\t\"birthdate\": null, \n\t\t\"city\": null, \n\t\t\"cuit\": null, \n\t\t\"date_joined\": \"2016-11-21T00:39:08.268478\", \n\t\t\"document\": null, \n\t\t\"email\": \"mail@server.com\", \n\t\t\"first_name\": null, \n\t\t\"floor\": null, \n\t\t\"gender\": null,\n\t\t\"id\": 14, \n\t\t\"is_active\": true, \n\t\t\"is_staff\": false, \n\t\t\"is_superuser\": false, \n\t\t\"last_login\": null, \n\t\t\"last_name\": null, \n\t\t\"phone\": null, \n\t\t\"street\": null, \n\t\t\"street_number\": null, \n\t\t\"unit\": null, \n\t\t\"username\": \"some_username2\"\n\t}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{\"username\": \"some_username2\", \"password\": 1234, \"email\": \"mail@server.com\"}' \nhttp://localhost:8000/api/v1/user/signup/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/reset-user/",
    "title": "Forgot password",
    "version": "0.0.1",
    "name": "ForgotPassword",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/:id/",
    "title": "Read data of a user",
    "version": "0.0.1",
    "name": "GetUser",
    "group": "User",
    "description": "<p>The user should be already logged, in other case you will get a 401 response.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "ID",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>M for Male or F for Female</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "birthdate",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "document",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "cuit",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "street_number",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "floor",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "area_code",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "phone",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/login/",
    "title": "Login user",
    "version": "0.0.1",
    "name": "LoginUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>M for Male or F for Female</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "birthdate",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "document",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "cuit",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "street_number",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "floor",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "area_code",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "phone",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Response (Error response example):",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Incorrect user\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/logout/",
    "title": "Logout user",
    "version": "0.0.1",
    "name": "LogoutUser",
    "group": "User",
    "description": "<p>The user should be already logged, in other case you will get a 401 response.</p>",
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user-preferences/update-user-preferences/",
    "title": "Update user preferences",
    "version": "0.0.1",
    "name": "UpdateUserPreferences",
    "group": "UserPreferences",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "category_ids",
            "description": "<p>List of category ids</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl --dump-header - -H \"Content-Type: application/json\" -X POST --data \n'{category_ids\": [1, 2]}'\nhttp://localhost:8000/api/v1/user-preferences/update-user-preferences/",
        "type": "curl"
      }
    ],
    "filename": "backend/apidoc.js",
    "groupTitle": "UserPreferences"
  },
  {
    "type": "get",
    "url": "/user-preferences/get-user-preferences/",
    "title": "User Preferences",
    "version": "0.0.1",
    "name": "UserPreferences",
    "group": "UserPreferences",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\"categories\": [{\"id\": 1, \"name\": \"Celulares\"}], \"id\": 1, \"user\": 1}",
          "type": "json"
        }
      ]
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "UserPreferences"
  },
  {
    "type": "get",
    "url": "/user/reputation/:id/",
    "title": "User reputation",
    "version": "0.0.1",
    "name": "ReputationUser",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "seller_reputation",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "buyer_reputation",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sales",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "pruchases",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/reset-password/",
    "title": "Reset password",
    "version": "0.0.1",
    "name": "ResetPassword",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/update-profile/:id/",
    "title": "Update profile",
    "version": "0.0.1",
    "name": "UpdateUser",
    "group": "User",
    "description": "<p>The user should be already logged, in other case you will get a 401 response.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>M for Male or F for Female</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "birthdate",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "document",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "cuit",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "street_number",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "floor",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "area_code",
            "description": "<ul> <li></li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "phone",
            "description": "<ul> <li></li> </ul>"
          }
        ]
      }
    },
    "filename": "backend/apidoc.js",
    "groupTitle": "User"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "backend/apidoc/main.js",
    "group": "_home_alejandro_projects_auctions_backend_apidoc_main_js",
    "groupTitle": "_home_alejandro_projects_auctions_backend_apidoc_main_js",
    "name": ""
  }
] });
