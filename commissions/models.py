# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Parameter(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True,
                            verbose_name='nombre')
    percentage = models.DecimalField(max_digits=10, decimal_places=2,
                                     verbose_name='porcentaje')

    class Meta:
        verbose_name = "Parámetro"
        verbose_name_plural = "Parametros"

    def __unicode__(self):
        return str(self.percentage)


class Commission(models.Model):
    payment_date = models.DateField(auto_now=True,
                                    verbose_name='fecha de operación')
    sale = models.ForeignKey('purchases.Purchase', verbose_name='venta')
    amount = models.DecimalField(max_digits=10, decimal_places=2,
                                 verbose_name='Comisión importe')

    def total(self):
        return self.sale.price

    def seller(self):
        return self.sale.seller.username
    seller.short_description = 'vendedor'

    def payment(self):
        return self.sale.get_payment_display()
    payment.short_description = 'compra forma de pago'

    class Meta:
        verbose_name = "Comisión"
        verbose_name_plural = "Comisiones"

    def __unicode__(self):
        return str(self.amount)
