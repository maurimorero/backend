from django.contrib import admin

from commissions.models import *

from rangefilter.filter import DateRangeFilter


class ParameterAdmin(admin.ModelAdmin):
    list_display = ('name', 'percentage')


class CommissionAdmin(admin.ModelAdmin):
    list_display = ('id', 'payment_date', 'total', 'seller', 'payment',
                    'amount')
    list_filter = (('payment_date', DateRangeFilter),)


admin.site.register(Parameter, ParameterAdmin)
admin.site.register(Commission, CommissionAdmin)
