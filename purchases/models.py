# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.db.models.signals import post_save
from django.dispatch import receiver

from products.models import Product, Price
from users.models import AuctionUser
from commissions.models import Commission, Parameter


class TypeSent(models.Model):
    name = models.CharField(max_length=100, verbose_name='nombre')

    class Meta:
        verbose_name = 'Tipo de envío'
        verbose_name_plural = 'Tipos de envio'

    def __unicode__(self):
        return self.name


class PurchaseState(models.Model):
    IN_PROGRESS = 'P'
    SENT = 'S'
    FINISHED = 'F'
    CANCELED = 'C'
    CONFLICT = 'W'

    state = models.CharField(max_length=1, choices=(
            (IN_PROGRESS, 'En progreso'),
            (SENT, 'Enviada'),
            (FINISHED, 'Exitosa'),
            (CONFLICT, 'En conflicto'),
            (CANCELED, 'Cancelada')
        ),
        verbose_name='estado', blank=True, null=True)
    type_sent = models.ForeignKey(TypeSent, null=True, blank=True,
                                  verbose_name='tipo de envío')
    tracking_code = models.CharField(max_length=100, null=True, blank=True,
                                     verbose_name='código de envío')

    class Meta:
        verbose_name = 'Estado de compra'
        verbose_name_plural = 'Estados de compras'

    def __unicode__(self):
        return self.get_state_display() if self.state else ''


class ShippingAddress(models.Model):
    city = models.CharField(max_length=100, null=True, blank=True,
                            verbose_name='ciudad')
    street = models.CharField(max_length=100, null=True, blank=True,
                              verbose_name='calle')
    street_number = models.IntegerField(null=True, blank=True,
                                        verbose_name='número')
    floor = models.IntegerField(null=True, blank=True, verbose_name='piso')
    unit = models.CharField(max_length=10, null=True, blank=True,
                            verbose_name='unidad')
    area_code = models.IntegerField(null=True, blank=True,
                                    verbose_name='código de area')

    class Meta:
        verbose_name = 'Dirección de envío'
        verbose_name_plural = 'Direcciones de envío'

    def __unicode__(self):
        return "Ciudad: {0}, calle: {1}, número".format(
            self.city, self.street, self.street_number)


class Purchase(models.Model):
    product = models.ForeignKey(Product, null=True, blank=True,
                                verbose_name='producto')
    offer = models.ForeignKey('estimations.Offer', null=True, blank=True,
                              verbose_name='oferta')
    date_created = models.DateField(auto_now_add=True, verbose_name='fecha')
    count = models.IntegerField(verbose_name='cantidad')
    buyer = models.ForeignKey(AuctionUser, related_name='purchases',
                              verbose_name='comprador')
    seller = models.ForeignKey(AuctionUser, related_name='sales',
                               verbose_name='vendedor')
    price = models.DecimalField(max_digits=10, decimal_places=2,
                                verbose_name='precio')
    price_data = models.ForeignKey(Price, null=True, blank=True)
    delivery = models.CharField(
        max_length=1,
        choices=(
            ('P', 'Pick up at seller address'),
            ('D', 'Door to door service / delivery')
        ), verbose_name='tipo de envio'
    )
    shipping_address = models.ForeignKey(
        ShippingAddress, null=True, blank=True,
        verbose_name='dirección de envío')
    state = models.ForeignKey(
        PurchaseState, null=True, blank=True, related_name='purchases',
        verbose_name='estado'
    )
    payment = models.CharField(
        max_length=1,
        choices=(
            ('C', 'Tarjeta'),
            ('E', 'Efectivo')
        ),
        null=True, blank=True, verbose_name='método de pago'
    )

    def get_receivers(self):
        data = []
        receivers = ReceivedPurchase.objects.filter(purchase=self)

        for receiver in receivers:
            data.append({
                'name': receiver.name,
            })
        return data

    def seller_full_name(self):
        return self.seller.get_full_name()
    seller_full_name.short_description = 'Vendedor nombre'

    def buyer_full_name(self):
        return self.buyer.get_full_name()
    buyer_full_name.short_description = 'Comprador nombre'

    class Meta:
        verbose_name = 'Compra'
        verbose_name_plural = 'Compras'

    def __unicode__(self):
        return "Compra #: {0}".format(self.id)


class ReceivedPurchase(models.Model):
    name = models.CharField(max_length=100, verbose_name='nombre')
    purchase = models.ForeignKey(
        Purchase, null=True, blank=True, related_name='receivers',
        verbose_name='compra'
    )

    class Meta:
        verbose_name = 'Recibe compra'
        verbose_name_plural = 'Recibidores de compras'

    def __unicode__(self):
        return self.name


# @receiver(post_save, sender=Purchase)
# def set_commision(sender, **kwargs):
#     parameter = Parameter.objects.first()
#
#     if parameter:
#         percentage = parameter.percentage
#
#         sale = kwargs['instance']
#
#         commission = Commission()
#         commission.sale = sale
#         commission.amount = sale.price * percentage
#         commission.save()


class SellerCalification(models.Model):
    calification_date = models.DateField(
        auto_now_add=True, null=True, blank=False, verbose_name='fecha'
    )
    purchase = models.ForeignKey(Purchase, related_name='seller_califications',
                                 verbose_name='compra')
    calification = models.IntegerField(verbose_name='calificación')
    comments = models.CharField(max_length=100, null=True, blank=True,
                                verbose_name='comentario')

    class Meta:
        verbose_name = 'Calificación vendedor'
        verbose_name_plural = 'Calificaciones de vendedores'

    def __unicode__(self):
        return "Compra: {0}, calificación: {1}".format(
            self.purchase, self.calification)


class BuyerCalification(models.Model):
    calification_date = models.DateField(
        auto_now_add=True, null=True, blank=False, verbose_name='fecha'
    )
    purchase = models.ForeignKey(Purchase, related_name='buyer_califications',
                                 verbose_name='compra')
    calification = models.IntegerField(verbose_name='calificación')
    comments = models.CharField(max_length=100, null=True, blank=True,
                                verbose_name='comentario')

    class Meta:
        verbose_name = 'Calificación comprador'
        verbose_name_plural = 'Calificaciones de compradores'

    def __unicode__(self):
        return "Compra: {0}, calificación: {1}".format(
            self.purchase, self.calification)
