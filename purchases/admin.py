from django.contrib import admin

from rangefilter.filter import DateRangeFilter

from purchases.models import *


class PurchaseAdmin(admin.ModelAdmin):
    list_display = ('id', 'date_created', 'count', 'price', 'buyer',
                    'buyer_full_name', 'seller', 'seller_full_name',
                     'state')
    list_filter = (('date_created', DateRangeFilter),
                   'state__state', 'product__category',
                   'product__sub_category', 'buyer', 'seller')
    search_fields = ['product__name', 'seller__username', 'buyer__username',
                     'seller__first_name', 'seller__last_name',
                     'buyer__first_name', 'buyer__last_name']

    def get_queryset(self, request):
        qset = super(PurchaseAdmin, self).get_queryset(request)
        return qset.filter(state__state__isnull=False).exclude(
            state__state=""
        )


class PurchaseStateAdmin(admin.ModelAdmin):
    list_display = ('state', 'type_sent', 'tracking_code')
    list_filter = ('state',)
    search_fields = ['user__username']


admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(SellerCalification)
admin.site.register(BuyerCalification)
admin.site.register(ShippingAddress)
admin.site.register(PurchaseState, PurchaseStateAdmin)
admin.site.register(TypeSent)
