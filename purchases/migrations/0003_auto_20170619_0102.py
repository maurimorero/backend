# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-06-19 01:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('purchases', '0002_auto_20170409_0129'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuyerCalification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('calification', models.IntegerField()),
                ('comments', models.CharField(blank=True, max_length=100, null=True)),
                ('purchase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='buyer_califications', to='purchases.Purchase')),
            ],
        ),
        migrations.CreateModel(
            name='SellerCalification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('calification', models.IntegerField()),
                ('comments', models.CharField(blank=True, max_length=100, null=True)),
                ('purchase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='seller_califications', to='purchases.Purchase')),
            ],
        ),
        migrations.RemoveField(
            model_name='purchasecalification',
            name='purchase',
        ),
        migrations.DeleteModel(
            name='PurchaseCalification',
        ),
    ]
