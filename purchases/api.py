# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site

from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication
from tastypie.exceptions import BadRequest

from purchases.models import *
from estimations.models import Offer, Estimation, CustomEstimation
from activitylog.models import UserActionLog
from products.models import Price
from commissions.models import Commission, Parameter

import datetime

import mercadopago


def ml_purchase(request, purchase, product, quoatation):
    mp = mercadopago.MP(purchase.seller.ml_access_token)

    name = ''
    if purchase.product:
        name = purchase.product.name
    elif purchase.offer.estimation:
        name = purchase.offer.estimation.product.name
    else:
        name = purchase.offer.custom_estimation.requested_product

    url_failure = None
    url_success = None
    url_pending = None

    if product:
        url_failure = 'http://' + str(
            get_current_site(request).domain) + '/product/detail/' + str(
            product.id) + '/'
        url_success = 'http://' + str(
            get_current_site(request).domain) + '/ml-success-purchase/' + str(
            purchase.id) + '/'
        url_pending = 'http://' + str(
            get_current_site(request).domain) + '/ml-pending-purchase/' + str(
            purchase.id) + '/'

    elif quoatation and not quoatation.is_custom_estimation():
        url_failure = 'http://' + str(
            get_current_site(request).domain) + '/quotation/detail/' + str(
            quoatation.id) + '/'
        url_success = 'http://' + str(
            get_current_site(request).domain) + '/ml-success-purchase/' + str(
            purchase.id) + '/'
        url_pending = 'http://' + str(
            get_current_site(request).domain) + '/ml-pending-purchase/' + str(
            purchase.id) + '/'
    else:
        url_failure = 'http://' + str(
            get_current_site(request).domain) + '/custom-quotation/detail/' + str(
            quoatation.id) + '/'
        url_success = 'http://' + str(
            get_current_site(request).domain) + '/ml-success-purchase/' + str(
            purchase.id) + '/'
        url_pending = 'http://' + str(
            get_current_site(request).domain) + '/ml-pending-purchase/' + str(
            purchase.id) + '/'

    percentage = Parameter.objects.first().percentage
    fee = None
    quantity = 1
    if not quoatation:
        fee = (float(purchase.price) * float(percentage)) * purchase.count
        quantity = purchase.count
    else:
        fee = (float(purchase.price) * float(percentage))

    preference = {
        "items": [
            {
                "title": name,
                "quantity": quantity,
                "currency_id": "ARS",
                "unit_price": float(purchase.price)
            }
        ],
        "marketplace_fee": fee,
        "notification_url": settings.NOTIFICATION_URL,
        "auto_return": "all",
        "back_urls": {
            "success": url_success,
            "pending": url_pending,
            "failure": url_failure
        },
        "payment_methods": {
            "excluded_payment_types": [{"id": "ticket"}]
        }
    }
    preferenceResult = mp.create_preference(preference)
    # NOTE: on prod use init_point
    return preferenceResult["response"]["init_point"]


class TypeSentResource(ModelResource):
    class Meta:
        queryset = TypeSent.objects.all()
        allowed_methods = ['get']
        resource_name = 'type-sent'
        authentication = SessionAuthentication()
        ordering = ['name']


class PurchaseResource(ModelResource):
    buyer = fields.CharField(attribute='buyer')
    buyer_id = fields.IntegerField(attribute='buyer__id')

    seller = fields.CharField(attribute='seller')
    seller_id = fields.IntegerField(attribute='seller__id')

    has_buyer_calification = fields.CharField()

    def dehydrate_has_buyer_calification(self, bundle):
        return bundle.obj.buyer_califications.all().exists()

    payment = fields.CharField()

    def dehydrate_payment(self, bundle):
        return bundle.obj.get_payment_display()

    class Meta:
        queryset = Purchase.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'purchase'
        authentication = SessionAuthentication()
        filtering = {
            'date_created': ALL,
            'buyer_id': ALL,
            'seller_id': ALL
        }
        ordering = ['date_created', 'price']

    # def obj_get_list(self, bundle, **kwargs):
    #     return Purchase.objects.filter(state__state__isnull=False)

    def build_filters(self, filters=None, **kwargs):
        if filters is None:
            filters = {}

        orm_filters = super(PurchaseResource, self).build_filters(filters)
        qset = Purchase.objects.filter(state__state__isnull=False).exclude(
            state__state=""
        )

        if 'seller_id' in filters:
            qset = qset.filter(seller__id=filters.get('seller_id'))
            orm_filters['id__in'] = [i.id for i in qset]

        if 'buyer_id' in filters:
            qset = qset.filter(buyer__id=filters.get('buyer_id'))
            orm_filters['id__in'] = [i.id for i in qset]

        if 'state' in filters and filters["state"]:
            qset = qset.filter(state__state=filters.get('state'))
            orm_filters['id__in'] = [i.id for i in qset]

        return orm_filters

    def dehydrate(self, bundle):
        bundle.data['seller_data'] = {
            'email': bundle.obj.seller.email,
            'name': bundle.obj.seller.get_full_name(),
            'phone': bundle.obj.seller.phone
        }

        bundle.data['buyer_data'] = {
            'email': bundle.obj.buyer.email,
            'name': bundle.obj.buyer.get_full_name(),
            'phone': bundle.obj.buyer.phone
        }

        bundle.data['receivers_data'] = bundle.obj.get_receivers()

        if bundle.obj.state:
            type_sent = 'En proceso'
            tracking_code = 'En proceso'

            if bundle.obj.state.type_sent:
                type_sent = bundle.obj.state.type_sent
            if bundle.obj.state.tracking_code:
                tracking_code = bundle.obj.state.tracking_code

            bundle.data['state'] = {
                'state': bundle.obj.state.get_state_display(),
                'type_sent': type_sent,
                'tracking_code': tracking_code
            }
        else:
            bundle.data['state'] = {}

        if bundle.obj.product:
            bundle.data['name'] = bundle.obj.product.name
            bundle.data['description'] = bundle.obj.product.description
            bundle.data['thumb_url_1'] = bundle.obj.product.get_thumb_1()
            bundle.data['thumb_url_2'] = bundle.obj.product.get_thumb_2()
            bundle.data['thumb_url_3'] = bundle.obj.product.get_thumb_3()
            bundle.data['thumb_url_4'] = bundle.obj.product.get_thumb_4()
            bundle.data['thumb_url_5'] = bundle.obj.product.get_thumb_5()
            bundle.data['thumb_url_6'] = bundle.obj.product.get_thumb_6()

            bundle.data['image_1'] = bundle.obj.product.image_1
            bundle.data['image_2'] = bundle.obj.product.image_2
            bundle.data['image_3'] = bundle.obj.product.image_3
            bundle.data['image_4'] = bundle.obj.product.image_4
            bundle.data['image_5'] = bundle.obj.product.image_5
            bundle.data['image_6'] = bundle.obj.product.image_6

            bundle.data['default_image'] = bundle.obj.product.sub_category.get_default_image()

            attributes = []
            for value in bundle.obj.product.attribute_values.all():
                attributes.append(
                    {
                        'name': value.attribute.name,
                        'value': value.value,
                        'feature': value.attribute.feature.name,
                        'group': value.attribute.feature.group.name
                    }
                )
            bundle.data['attributes'] = attributes

        elif type(bundle.obj.offer.get_quoatation()) == Estimation:
            quoatation = bundle.obj.offer.get_quoatation()
            product = quoatation.product
            bundle.data['name'] = product.name
            bundle.data['thumb_url_1'] = product.get_thumb_1()
            bundle.data['thumb_url_2'] = product.get_thumb_2()
            bundle.data['thumb_url_3'] = product.get_thumb_3()
            bundle.data['thumb_url_4'] = product.get_thumb_4()
            bundle.data['thumb_url_5'] = product.get_thumb_5()
            bundle.data['thumb_url_6'] = product.get_thumb_6()

            bundle.data['image_1'] = product.image_1
            bundle.data['image_2'] = product.image_2
            bundle.data['image_3'] = product.image_3
            bundle.data['image_4'] = product.image_4
            bundle.data['image_5'] = product.image_5
            bundle.data['image_6'] = product.image_6

            bundle.data['default_image'] = product.sub_category.get_default_image()

        elif type(bundle.obj.offer.get_quoatation()) == CustomEstimation:
            quoatation = bundle.obj.offer.get_quoatation()
            bundle.data['name'] = quoatation.requested_product
            bundle.data['default_image'] = quoatation.sub_category.get_default_image()

        bundle.data['complaints'] = []
        for complaint in bundle.obj.complaints.all():
            bundle.data['complaints'].append(
                {
                    'id': complaint.id,
                    'date': complaint.date,
                    'state': complaint.get_state_display(),
                    'complaint_type': complaint.get_complaint_type_display(),
                    'motive': complaint.motive
                }
            )

        return bundle

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/create-purchase%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_purchase'),
                name="api_create_purchase"
            ),
            url(
                r"^(?P<resource_name>%s)/seller-calification%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('seller_calification'),
                name="api_seller_calification"
            ),
            url(
                r"^(?P<resource_name>%s)/buyer-calification%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('buyer_calification'),
                name="api_buyer_calification"
            ),
            url(
                r"^(?P<resource_name>%s)/set-state-to-sent%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('purchase_set_state_to_sent'),
                name="api_purchase_set_state_to_sent"
            ),
            url(
                r"^(?P<resource_name>%s)/ml-update-state%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('ml_update_state'),
                name="api_ml_update_state"
            )
        ]

    def create_purchase(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        product_id = data.get('product_id')
        count = data.get('count')
        buyer_id = data.get('buyer_id')
        seller_id = data.get('seller_id')
        price_id = data.get('price_id', '')
        offer_id = data.get('offer_id', '')
        delivery = data.get('delivery')
        receivers = data.get('receivers', [])
        payment = data.get('payment', 'E')

        product = None
        quoatation = None

        purchase = Purchase()
        purchase.date_successfully = datetime.date.today()

        if product_id:
            product = Product.objects.get(id=product_id)
            purchase.product = product

        if price_id:
            price = Price.objects.get(id=price_id)
            purchase.price = price.price
            purchase.count = count
            purchase.price_data = price

        elif offer_id:
            offer = Offer.objects.get(id=offer_id)
            quoatation = offer.get_quoatation()
            purchase.offer = offer
            purchase.price = offer.price
            purchase.count = quoatation.count
            quoatation.state = quoatation.SUCCESSFULLY
            quoatation.save()

        purchase.buyer = AuctionUser.objects.get(id=buyer_id)
        purchase.seller = AuctionUser.objects.get(id=seller_id)
        purchase.delivery = delivery
        purchase.payment = payment

        state = PurchaseState()
        state.save()
        purchase.state = state

        purchase.save()

        UserActionLog(
            user=purchase.buyer,
            action=UserActionLog.PURCHASE,
            description="Compra ID: %s" % purchase.id
        ).save()

        UserActionLog(
            user=purchase.seller,
            action=UserActionLog.SALE,
            description="Compra ID: %s" % purchase.id
        ).save()

        # Set shipping address
        if purchase.delivery == 'D':
            city = data.get('city')
            street = data.get('street')
            street_number = data.get('street_number')
            floor = data.get('floor')
            unit = data.get('unit')
            area_code = data.get('area_code')

            shipping_address = ShippingAddress()
            shipping_address.purchase = purchase
            shipping_address.city = city
            shipping_address.street = street
            shipping_address.street_number = street_number
            shipping_address.floor = floor
            shipping_address.unit = unit
            shipping_address.area_code = area_code

            shipping_address.save()

        for receiver in receivers:
            ReceivedPurchase(name=receiver, purchase=purchase).save()

        payment_url_ml = ml_purchase(request, purchase, product, quoatation)
        print payment_url_ml

        return self.create_response(request, {
            'payment_url_ml': payment_url_ml}
        )

    def seller_calification(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        calification_number = data.get('calification')
        purchase_id = data.get('purchase_id')
        comments = data.get('comments')

        purchase = Purchase.objects.get(id=purchase_id)
        state = purchase.state
        state.state = PurchaseState.FINISHED
        state.save()

        calification = SellerCalification()
        calification.calification = calification_number
        calification.purchase = purchase
        calification.comments = comments

        calification.save()

        return self.create_response(request, {})

    def buyer_calification(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        calification_number = data.get('calification')
        purchase_id = data.get('purchase_id')
        comments = data.get('comments')

        purchase = Purchase.objects.get(id=purchase_id)

        calification = BuyerCalification()
        calification.calification = calification_number
        calification.purchase = purchase
        calification.comments = comments

        calification.save()

        return self.create_response(request, {})

    def purchase_set_state_to_sent(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        purchase_id = data.get('purchase_id')
        type_sent_id = data.get('type_sent_id')
        tracking_code = data.get('tracking_code', '')

        purchase = Purchase.objects.get(id=purchase_id)
        state = purchase.state

        state.state = PurchaseState.SENT
        state.type_sent = TypeSent.objects.get(id=type_sent_id)
        state.tracking_code = tracking_code
        state.save()

        return self.create_response(request, {})

    def ml_update_state(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        if not request.user.is_authenticated():
            raise BadRequest("Authorization error")

        data = self.deserialize(request, request.body, format=request.META.get(
            'CONTENT_TYPE', 'application/json')
        )

        purchase_id = data.get('purchase_id')
        ml_state = data.get('state')

        purchase = Purchase.objects.get(id=purchase_id)
        state = purchase.state
        price = None

        if ml_state == "success":
            state.state = PurchaseState.IN_PROGRESS

            if purchase.price_data:
                price = purchase.price_data
                price.stock = price.stock - purchase.count
                price.save()

            parameter = Parameter.objects.first()

            if parameter:
                percentage = parameter.percentage

                commission = Commission()
                commission.sale = purchase
                if price:
                    commission.amount = (purchase.price * percentage) * purchase.count
                else:
                    commission.amount = (purchase.price * percentage)
                commission.save()

        state.save()

        return self.create_response(request, {})
