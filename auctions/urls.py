"""auctions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import admin_reports

from tastypie.api import Api

from users.api import UserResource
from products.api import (CategoryResource, SubCategoryResource,
                          ProductResource, UserPreferencesResource
                          )
from estimations.api import (EstimationResource, QuestionResource,
                             CustomEstimationResource, OfferResource)
from purchases.api import PurchaseResource, TypeSentResource
from activitylog.api import UserActionLogResource
from complaints.api import ComplaintResource, ComplaintQuestionResource
from site_configuration.api import SiteConfigurationResource, ContactResource

from purchases.views import mp_notification

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(CategoryResource())
v1_api.register(SubCategoryResource())
v1_api.register(ProductResource())
v1_api.register(EstimationResource())
v1_api.register(CustomEstimationResource())
v1_api.register(PurchaseResource())
v1_api.register(UserPreferencesResource())
v1_api.register(QuestionResource())
v1_api.register(UserActionLogResource())
v1_api.register(TypeSentResource())
v1_api.register(OfferResource())
v1_api.register(ComplaintResource())
v1_api.register(ComplaintQuestionResource())
v1_api.register(SiteConfigurationResource())
v1_api.register(ContactResource())


urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    # url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin.site.urls),
    url(r'^admin/', include(admin_reports.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'^mp-notification', mp_notification)
]

urlpatterns += staticfiles_urlpatterns()
