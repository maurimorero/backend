# -*- coding: utf-8 -*-
from django.contrib import admin
from admin_reports import Report, register

from users.models import AuctionUser
from products.models import Product, Price
from complaints.models import Complaint
from purchases.models import Purchase
from estimations.models import Estimation, CustomEstimation, Offer, Quotation

import math


@register()
class Report(Report):
    title = 'Reporte'
    template_name = "custom_report.html"

    fields = [
        'usuarios',
        'cotizaciones',
        'ofertas',
        'productos',
        'precios',
        'compras',
        'reclamos',

        'efectividad',
        'reclamos_proporcion',
        'duracion',
        'conversion'
    ]

    alignment = {
        'usuarios': 'top',
        'cotizaciones': 'top',
        'ofertas': 'top',
        'productos': 'top',
        'precios': 'top',
        'compras': 'top',
        'reclamos': 'top',

        'efectividad': 'botom-efectividad',
        'reclamos_proporcion': 'botom-reclamos-proporcion',
        'duracion': 'botom-duracion',
        'conversion': 'botom-conversion'
    }

    def reclamos_proporcion(self, record):
        return str(record['reclamos_proporcion']) + "%"

    def efectividad(self, record):
        return str(record['efectividad']) + "%"

    def aggregate(self, **kwargs):
        count_estimation = Estimation.objects.all().count()
        count_custom_estimation = + CustomEstimation.objects.all().count()

        purchases = Purchase.objects.all()
        quatations = Quotation.objects.all()

        count_quatations = count_estimation + count_custom_estimation
        count_purchases = purchases.count()
        count_purchases_from_quatations = purchases.filter(
            offer__isnull=False).count()
        complaints_count = Complaint.objects.all().count()

        days_quatation = []
        for q in quatations:
            days_quatation.append((q.date_end - q.date_created).days)
        quatations_duration = 0
        if days_quatation:
            quatations_duration = sum(days_quatation) / len(days_quatation)

        days_convertion = []
        for q in quatations:
            if q.date_successfully:
                days_convertion.append(
                    (q.date_successfully - q.date_created).days
                )
        convertion = 0
        if days_convertion:
            convertion = sum(days_convertion) / len(days_convertion)

        return [
            {
                'usuarios': AuctionUser.objects.all().count(),
                'cotizaciones': count_estimation + count_custom_estimation,
                'ofertas': Offer.objects.all().count(),
                'productos': Product.objects.all().count(),
                'precios': Price.objects.all().count(),
                'compras': Purchase.objects.all().count(),
                'reclamos': Complaint.objects.all().count(),
                'efectividad': round(
                    count_purchases_from_quatations / float(count_quatations),
                    2) * 100,
                'reclamos_proporcion': round(
                    complaints_count / float(count_purchases), 2) * 100,
                'duracion': quatations_duration,
                'conversion': convertion
            }
        ]
