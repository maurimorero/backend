/**
 * @api {get} /user/:id/ Read data of a user
 * @apiVersion 0.0.1
 * @apiName GetUser
 * @apiGroup User
 * @apiDescription The user should be already logged, in other case you will get a 401 response.
 *
 * @apiSuccess {Integer}  ID      -
 * @apiSuccess {String}   username      -
 * @apiSuccess {String}   email         -
 * @apiSuccess {String}   last_name     -
 * @apiSuccess {String}   first_name    -
 * @apiSuccess {String}   gender        M for Male or F for Female
 * @apiSuccess {Date}     birthdate     -
 * @apiSuccess {Integer}  document      -
 * @apiSuccess {Integer}  cuit          -
 * @apiSuccess {String}   city          -
 * @apiSuccess {String}   street        -
 * @apiSuccess {Integer}  street_number -
 * @apiSuccess {Integer}  floor         -
 * @apiSuccess {String}   unit          
 * @apiSuccess {Integer}  area_code     -
 * @apiSuccess {Integer}  phone         -
 *
 */

/**
 * @api {get} /user/reputation/:id/ User reputation
 * @apiVersion 0.0.1
 * @apiName ReputationUser
 * @apiGroup User
 *
 * @apiSuccess {Integer}  seller_reputation      -
 * @apiSuccess {Integer}  buyer_reputation       -
 * @apiSuccess {Integer}  sales                  -
 * @apiSuccess {Integer}  pruchases              -
 */

 /**
 * @api {post} /user/update-profile/:id/ Update profile
 * @apiVersion 0.0.1
 * @apiName UpdateUser
 * @apiGroup User
 * @apiDescription The user should be already logged, in other case you will get a 401 response.
 *
 * @apiParam {String}   username      -
 * @apiParam {String}   email         -
 * @apiParam {String}   last_name     -
 * @apiParam {String}   first_name    -
 * @apiParam {String}   gender        M for Male or F for Female
 * @apiParam {Date}     birthdate     -
 * @apiParam {Integer}  document      -
 * @apiParam {Integer}  cuit          -
 * @apiParam {String}   city          -
 * @apiParam {String}   street        -
 * @apiParam {Integer}  street_number -
 * @apiParam {Integer}  floor         -
 * @apiParam {String}   unit          
 * @apiParam {Integer}  area_code     -
 * @apiParam {Integer}  phone         -
 */

 /**
 * @api {post} /user/signup/ Create user
 * @apiVersion 0.0.1
 * @apiName CreateUser
 * @apiGroup User
 *
 * @apiParam {String}   username                -
 * @apiParam {String}   email                   -
 * @apiParam {String}   password                -
 * @apiParam {String}   last_name               (optional)
 * @apiParam {String}   first_name              (optional)
 *
 * @apiErrorExample Response (Error response example):
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Nombre de usuario no disponible"
 *     }
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *		"area_code": null,
		"birthdate": null, 
		"city": null, 
		"cuit": null, 
		"date_joined": "2016-11-21T00:39:08.268478", 
		"document": null, 
		"email": "mail@server.com", 
		"first_name": null, 
		"floor": null, 
		"gender": null,
		"id": 14, 
		"is_active": true, 
		"is_staff": false, 
		"is_superuser": false, 
		"last_login": null, 
		"last_name": null, 
		"phone": null, 
		"street": null, 
		"street_number": null, 
		"unit": null, 
		"username": "some_username2"
 *	}
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"username": "some_username2", "password": 1234, "email": "mail@server.com"}' 
 *     http://localhost:8000/api/v1/user/signup/
 */

 /**
 * @api {post} /user/login/ Login user
 * @apiVersion 0.0.1
 * @apiName LoginUser
 * @apiGroup User
 *
 * @apiParam {String} username -
 * @apiParam {String} password -
 *
 * @apiSuccess {String}   username      -
 * @apiSuccess {String}   email         -
 * @apiSuccess {String}   last_name     -
 * @apiSuccess {String}   first_name    -
 * @apiSuccess {String}   gender        M for Male or F for Female
 * @apiSuccess {Date}     birthdate     -
 * @apiSuccess {Integer}  document      -
 * @apiSuccess {Integer}  cuit          -
 * @apiSuccess {String}   city          -
 * @apiSuccess {String}   street        -
 * @apiSuccess {Integer}  street_number -
 * @apiSuccess {Integer}  floor         -
 * @apiSuccess {String}   unit          
 * @apiSuccess {Integer}  area_code     -
 * @apiSuccess {Integer}  phone         -
 *
 * @apiErrorExample Response (Error response example):
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "error": "Incorrect user"
 *     }
 */

 /**
 * @api {get} /user/logout/ Logout user
 * @apiVersion 0.0.1
 * @apiName LogoutUser
 * @apiGroup User
 * @apiDescription The user should be already logged, in other case you will get a 401 response.
 */

 /**
 * @api {post} /user/change-password/ Change password
 * @apiVersion 0.0.1
 * @apiName ChangePassword
 * @apiGroup User
 *
 * @apiParam {String} password -
 */

  /**
 * @api {post} /user/activate-user/ Activate user
 * @apiVersion 0.0.1
 * @apiName ActivateUser
 * @apiGroup User
 *
 * @apiParam {String} code -
 * @apiParam {String} user_id -
 */

 /**
 * @api {post} /user/reset-user/ Forgot password
 * @apiVersion 0.0.1
 * @apiName ForgotPassword
 * @apiGroup User
 *
 * @apiParam {String} email -
 */

 /**
 * @api {post} /user/reset-password/ Reset password
 * @apiVersion 0.0.1
 * @apiName ResetPassword
 * @apiGroup User
 *
 * @apiParam {String} code -
 * @apiParam {String} password -
 */

 /**
 * @api {get} /category/ Category list
 * @apiVersion 0.0.1
 * @apiName CategoryList
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Tecnologia"
 *     },
 *     {
 *       "id": 2,
 *       "name": "Electrodomesticos"
 *     }
 */

 /**
 * @api {get} /category/?name__contains=CategoryName Category list search/filter
 * @apiVersion 0.0.1
 * @apiName CategoryListFilter
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Tecnologia"
 *     },
 *     {
 *       "id": 2,
 *       "name": "Electrodomesticos"
 *     }
 * @apiExample {curl} Example usage:
 *     curl -H "Accept: application/json" http://localhost:8000/api/v1/category/?name__contains=Electrodomesticos
 */

 /**
 * @api {get} /subcategory/ SubCategory list
 * @apiVersion 0.0.1
 * @apiName SubCategoryList
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Celulares",
 *       "category": "Tegnologia",
 *       "category_id": 1,
 *     },
 *     {
 *       "id": 2,
 *       "name": "Televisores",
 *       "category": "Tegnologia",
 *       "category_id": 1,
 *     }
 */

 /**
 * @api {get} /subcategory/?name__contains=SubCategoryName SubCategory list search/filter by name
 * @apiVersion 0.0.1
 * @apiName SubCategoryListFilterByName
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Celulares"
 *       "category": "Tegnologia",
 *       "category_id": 1,
 *     },
 *     {
 *       "id": 2,
 *       "name": "Televisores",
 *       "category": "Tegnologia",
 *       "category_id": 1,
 *     }
 * @apiExample {curl} Example usage:
 *     curl -H "Accept: application/json" http://localhost:8000/api/v1/subcategory/?name__contains=Celulares
 */

 /**
 * @api {get} /subcategory/?category_id=ID SubCategory list filter by category
 * @apiVersion 0.0.1
 * @apiName SubCategoryListFilterByCategory
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Celulares"
 *       "category": "Tegnologia",
 *       "category_id": 1,
 *     },
 *     {
 *       "id": 2,
 *       "name": "Televisores",
 *       "category": "Tegnologia",
 *       "category_id": 1,
 *     }
 */

 /**
 * @api {get} /product/?category_id=ID&sub_category_id=ID&name__contains=Something Product list
 * @apiVersion 0.0.1
 * @apiName ProductList
 * @apiGroup Product
 * @apiDescription If call the endpoint without parameters (name, category_id, or sub_category_id), then returns all products.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *			"category": "Tegnologia",
 *			"category_id": 1, 
 *			"id": 1, 
 *			"name": "Nokia", 
 *			"prices": [{"id": 1, "seller_name": "Cosme Fulanito"}], 
 *			"sub_category": "Celulares", 
 *			"sub_category_id": 1
 *		}, 
 *		{
 *			"category": "Tegnologia", 
 *			"category_id": 1, 
 *			"id": 2, 
 *			"name": "Iphone", 
 *			"prices": [], 
 *			"sub_category": "Celulares", 
 *			"sub_category_id": 1
 *		}, 
 *		{
 *			"category": "Electrodomesticos", 
 *			"category_id": 2, 
 *			"id": 3, 
 *			"name": "Samsung 20 pulgadas", 
 *			"prices": [], 
 *			"sub_category": "Televisores", 
 *			"sub_category_id": 2
 *		}
 * @apiExample {curl} Example usage:
 *     curl -H "Accept: application/json" 
 *     http://localhost:8000/api/v1/product/?category_id=2&sub_category_id=2&name__contains=Samsung
 *
 *     curl -H "Accept: application/json" 
 *     http://localhost:8000/api/v1/product/?category_id=1&sub_category_id=1
 *
 *
 *     curl -H "Accept: application/json" 
 *     http://localhost:8000/api/v1/product/?category_id=1
 *
 *
 *     curl -H "Accept: application/json" 
 *     http://localhost:8000/api/v1/product/
 */

 /**
 * @api {get} /product/ID/ Product detail
 * @apiVersion 0.0.1
 * @apiName ProductDetail
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *			"category": "Tegnologia",
 *			"category_id": 1, 
 *			"id": 1, 
 *			"name": "Nokia", 
 *			"prices": [{"id": 1, "seller_name": "Cosme Fulanito"}], 
 *			"sub_category": "Celulares", 
 *			"sub_category_id": 1,
 *			"attributes": [{"name": "Sistema Operativo", "value": "Android"}]
 *		}
 */

 /**
 * @api {get} /product/get-product-attributes/PRODUCT-ID/ List product attributes
 * @apiVersion 0.0.1
 * @apiName ListProductAttributtes
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {"group": 1, "group_name": "Informacion del producto", "id": 1, "name": "Pantalla", "sub_category": 1}, 
 *     {"group": 1, "group_name": "Informacion del producto", "id": 2, "name": "Sistema operativo", "sub_category": 1}
 */

 /**
 * @api {post} /product/set-product-attributes/PRODUCT-ID/ Set product attributes
 * @apiVersion 0.0.1
 * @apiName SetProductAttributes
 * @apiGroup Product
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"attrs": [{"value": "Android", "attribute_id": 2}]}' 
 *     http://localhost:8000/api/v1/product/set-product-attributes/PRODUCTO-ID/
 */

 /**
 * @api {post} /product/set-price/ Set price
 * @apiVersion 0.0.1
 * @apiName SetPrice
 * @apiGroup Product
 *
 * @apiParam {Integer} seller_id -
 * @apiParam {Integer} product_id -
 * @apiParam {String} price -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"seller_id": 1, "price": "2.80", "product_id": 1}'
 *     http://localhost:8000/api/v1/product/set-price/
 */

 /**
 * @api {post} /product/load-product/ Load product
 * @apiVersion 0.0.1
 * @apiName LoadProduct
 * @apiGroup Product
 *
 * @apiParam {Integer} category_id    -
 * @apiParam {Integer} sub_category_id -
 * @apiParam {Integer} name           -
 * @apiParam {Integer} description    -
 * @apiParam {Integer} state          N for new, or U for used
 * @apiParam {String}  price           -
 * @apiParam {Base64}  image_1        Optional
 * @apiParam {Base64}  image_2        Optional
 * @apiParam {Base64}  image_3        Optional
 * @apiParam {Base64}  image_4        Optional
 * @apiParam {Base64}  image_5        Optional
 * @apiParam {Base64}  image_6        Optional
 */

 /**
 * @api {get} /product/?user=ID Get products loaded by the user
 * @apiVersion 0.0.1
 * @apiName GetProductsLoaded
 * @apiGroup Product
 */

 /**
 * @api {post} /product/set-price/ Set price
 * @apiVersion 0.0.1
 * @apiName SetPrice
 * @apiGroup Product
 *
 * @apiParam {Integer} seller_id -
 * @apiParam {Integer} product_id -
 * @apiParam {String} price -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"seller_id": 1, "price": "2.80", "product_id": 1}'
 *     http://localhost:8000/api/v1/product/set-price/
 */

  /**
 * @api {post} /product/set-product-as-favorite/ Set product as favorite
 * @apiVersion 0.0.1
 * @apiName SetProductAsFavorite
 * @apiGroup Product
 *
 * @apiParam {Integer} product_id -
 */

 /**
 * @api {get} /product/get-favorite-products/ Get user favorite products
 * @apiVersion 0.0.1
 * @apiName GetUserFavoriteProducts
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "category": "Tegnologia", 
 *         "category_id": 1, 
 *         "description": "", 
 *         "id": 1, 
 *         "image_1": "static/image/5132014122216PM_635_motorola_moto_e.jpeg", 
 *         "image_2": "", 
 *         "image_3": "", 
 *         "image_4": "", 
 *         "image_5": "", 
 *         "image_6": "", 
 *         "name": "Moto E", 
 *         "state": "U", 
 *         "sub_category": "Celulares", 
 *         "sub_category_id": 1, 
 *         "thumb_url_1": "static/image/5132014122216PM_635_motorola_moto_e.jpeg.340x200_q85_crop.jpg", 
 *         "thumb_url_2": "", 
 *         "thumb_url_3": "", 
 *         "thumb_url_4": "", 
 *         "thumb_url_5": "", 
 *         "thumb_url_6": ""
 *    },
 {
 *         "category": "Tegnologia", 
 *         "category_id": 1, 
 *         "description": "", 
 *         "id": 1, 
 *         "image_1": "static/image/5132014122216PM_635_motorola_moto_e.jpeg", 
 *         "image_2": "", 
 *         "image_3": "", 
 *         "image_4": "", 
 *         "image_5": "", 
 *         "image_6": "", 
 *         "name": "Moto E", 
 *         "state": "U", 
 *         "sub_category": "Celulares", 
 *         "sub_category_id": 1, 
 *         "thumb_url_1": "static/image/5132014122216PM_635_motorola_moto_e.jpeg.340x200_q85_crop.jpg", 
 *         "thumb_url_2": "", 
 *         "thumb_url_3": "", 
 *         "thumb_url_4": "", 
 *         "thumb_url_5": "", 
 *         "thumb_url_6": ""
 *    },
 */

 /**
 * @api {get} /product/?user_id=USER_ID/ Product filter by user preferences
 * @apiVersion 0.0.1
 * @apiName ProductListByUserPreferences
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *			"category": "Tegnologia",
 *			"category_id": 1, 
 *			"id": 1, 
 *			"name": "Nokia", 
 *			"prices": [{"id": 1, "seller_name": "Cosme Fulanito"}], 
 *			"sub_category": "Celulares", 
 *			"sub_category_id": 1
 *		}, 
 *		{
 *			"category": "Tegnologia", 
 *			"category_id": 1, 
 *			"id": 2, 
 *			"name": "Iphone", 
 *			"prices": [], 
 *			"sub_category": "Celulares", 
 *			"sub_category_id": 1
 *		}
 */

/**
 * @api {get} /product/get-user-products/ Get user products
 * @apiVersion 0.0.1
 * @apiName UserProductList
 * @apiGroup Product
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *      [{
 *          "category": Tegnologia, 
 *          "category_id": 1,
 *          "sub_category": "Tegnologia", 
 *          "sub_category_id": 1
 *          "description": "Moto E segunda generaciÃ³n", 
 *          "id": 1, 
 *          "image": "", 
 *          "name": "Moto E", 
 *          "price": "5.00", 
 *          "state": null, 
 *          "sub_category": 1
 *      }]
 */

/**
 * @api {get} /user-preferences/get-user-preferences/ User Preferences
 * @apiVersion 0.0.1
 * @apiName UserPreferences
 * @apiGroup UserPreferences
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {"categories": [{"id": 1, "name": "Celulares"}], "id": 1, "user": 1}
 */

 /**
 * @api {post} /user-preferences/update-user-preferences/ Update user preferences
 * @apiVersion 0.0.1
 * @apiName UpdateUserPreferences
 * @apiGroup UserPreferences
 *
 * @apiParam {Integer} category_ids List of category ids
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{category_ids": [1, 2]}'
 *     http://localhost:8000/api/v1/user-preferences/update-user-preferences/
 */

 /**
 * @api {get} /estimation/ Estimation list
 * @apiVersion 0.0.1
 * @apiName EstimationList
 * @apiGroup Estimation
 * @apiDescription Returns all estimations
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *		"is_active": false,
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"date_end": "2016-10-28", 
 *		"id": 1, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"comments": "",
 *      "offers": [{"id": 1, "location": "", "price": "123123.00", "seller": "Alejandro Dev", "seller_id": 1}]
 *	}, 
 *	{
 *		"is_active": true, 
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"date_end": "2016-10-28", 
 *		"id": 2, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"comments": "",
 *      "offers": [{"id": 1, "location": "", "price": "123123.00", "seller": "Alejandro Dev", "seller_id": 1}]
 *	}, 
 *	{
 *		"active": true, 
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"date_end": "2016-10-28", 
 *		"id": 3, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"comments": "",
 *      "offers": [{"id": 1, "location": "", "price": "123123.00", "seller": "Alejandro Dev", "seller_id": 1}]
 *	}
 */

 /**
 * @api {get} /estimation/?active Estimation list filter by active
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterByActive
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?with-offers=True Estimation list filter with offers
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterWithOffers
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?with-offers=False Estimation list filter without offers
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterWithoutOffers
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?expired=True|False Estimation list filter expired
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterExpired
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?successful=True Estimation list filter successful
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterSuccessful
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?canceled=True Estimation list filter canceled
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterCanceled
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?order_by=date_created Estimation list order by date created
 * @apiVersion 0.0.1
 * @apiDescription For descending order use -date_created
 * @apiName EstimationListOrderByDateCreated
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?order_by=date_end Estimation list order by date end
 * @apiVersion 0.0.1
 * @apiDescription For descending order use -date_end
 * @apiName EstimationListOrderByDateEnd
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?buyer__id=ID Only my estimations
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterByOnlyMyselfEstimations
 * @apiGroup Estimation
 */

/**
 * @api {get} /estimation/?buyer__id=ID&active=True|False User active or Inactive Estimations
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterByActiveOrInactiveEstimations
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/?cateogry=ID Filter by Category
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterByCategory
 * @apiGroup Estimation
 */

  /**
 * @api {get} /estimation/?sub_cateogry=ID Filter by SubCategory
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterBySubCategory
 * @apiGroup Estimation
 */

 /**
 * @api {get} /estimation/ID/ Estimation detail
 * @apiVersion 0.0.1
 * @apiName EstimationDetail
 * @apiGroup Estimation
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *		"is_active": true, 
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"date_end": "2016-10-28", 
 *		"id": 1, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"comments": "",
 *	}
 */

 /**
 * @api {get} /custom-estimation/ Custom Estimation list
 * @apiVersion 0.0.1
 * @apiName CustomEstimationList
 * @apiGroup CustomEstimation
 * @apiDescription Returns all custom estimations
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *{
 *     "buyer": "admin",
 *     "buyer_id": 1,
 *     "category": "Celulares",
 *     "category_id": 1,
 *     "comments": "some comment",
 *     "count": 1,
 *     "date_created": "2016-12-15",
 *     "date_end": "2016-10-28",
 *     "id": 1,
 *     "is_active": false,
 *     "offers": [
 *       {
 *         "id": 2,
 *         "location": "",
 *         "price": "2123123.00",
 *         "seller": "Alejandro Dev",
 *         "seller_id": 1
 *       }
 *     ],
 *     "product_description": null,
 *     "requested_product": "Algun producto custom",
 *     "sub_category": "Tegnologia",
 *     "sub_category_id": 1
 *   }
 */

 /**
 * @api {get} /custom-estimation/?active CustomEstimation list filter by active
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterByActive
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?with-offers=True CustomEstimation list filter with offers
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterWithOffers
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?with-offers=False CustomEstimation list filter without offers
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterWithoutOffers
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?expired=True|False CustomEstimation list filter expired
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterExpired
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?successful=True CustomEstimation list filter successful
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterSuccessful
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?canceled=True CustomEstimation list filter canceled
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterCanceled
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?order_by=date_created CustomEstimation list order by date created
 * @apiVersion 0.0.1
 * @apiDescription For descending order use -date_created
 * @apiName CustomEstimationListOrderByDateCreated
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?order_by=date_end CustomEstimation list order by date end
 * @apiVersion 0.0.1
 * @apiDescription For descending order use -date_end
 * @apiName CustomEstimationListOrderByDateEnd
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?buyer__id=ID Only my custom estimations
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterByOnlyMyselfEstimations
 * @apiGroup CustomEstimation
 */

/**
 * @api {get} /custom-estimation/?buyer__id=ID&active=True|False User active or Inactive CustomEstimation
 * @apiVersion 0.0.1
 * @apiName EstimationListFilterByActiveOrInactiveEstimations
 * @apiGroup CustomEstimation
 */

 /**
 * @api {get} /custom-estimation/?cateogry=ID Filter by Category
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterByCategory
 * @apiGroup CustomEstimation
 */

  /**
 * @api {get} /custom-estimation/?sub_cateogry=ID Filter by SubCategory
 * @apiVersion 0.0.1
 * @apiName CustomEstimationListFilterBySubCategory
 * @apiGroup CustomEstimation
 */

 /**
 * @api {post} /custom-estimation/create-custom-estimation/ Custom Estimation create
 * @apiVersion 0.0.1
 * @apiName CustomEstimationCreate
 * @apiGroup CustomEstimation
 * @apiDescription The format date MUST be YYYY-MM-DD
 *
 * @apiParam {String} requested_product -
 * @apiParam {Integer} count -
 * @apiParam {String} buyer_id -
 * @apiParam {String} comments -
 * @apiParam {Integer} category_id -
 * @apiParam {Integer} sub_category_id -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     {"date_end":"2016-10-28", "count":1, "buyer_id":1,"comments": "some comment", 
 *     "requested_product": "Un prodcuto custom", "category_id": 1, "sub_category_id": 1}
 *     http://localhost:8000/api/v1/custom-estimation/create-custom-estimation/
 */

 /**
 * @api {get} /custom-estimation/custom-estimation-create-offer/ Create offer for custom estimation
 * @apiVersion 0.0.1
 * @apiName CustomEstimationCreateOffer
 * @apiGroup CustomEstimation
 *
 * @apiParam {Integer} seller_id -
 * @apiParam {Integer} estmation_id -
 * @apiParam {String} price -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"seller_id": 1, "estimation_id": 1, "price": "2.45"}'
 *     http://localhost:8000/api/v1/custom-estimation/custom-estimation-create-offer/
 */

 /**
 * @api {get} /custom-estimations/get-estimation-attributes/ID/ List custom estimations attributes
 * @apiVersion 0.0.1
 * @apiName ListCustomEstimationAttributtes
 * @apiGroup CustomEstimation
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {"group": 1, "group_name": "Informacion del producto", "id": 1, "name": "Pantalla", "sub_category": 1}, 
 *     {"group": 1, "group_name": "Informacion del producto", "id": 2, "name": "Sistema operativo", "sub_category": 1}
 */

 /**
 * @api {post} /custom-estimation/set-estimation-attributes/ID/ Set product attributes
 * @apiVersion 0.0.1
 * @apiName SetCustomEstimationAttributes
 * @apiGroup CustomEstimation
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"attrs": [{"value": "Android", "attribute_id": 2}]}' 
 *     http://localhost:8000/api/v1/product/set-product-attributes/PRODUCTO-ID/
 */

/**
 * @api {get} /purchase/?buyer_id=ID Purchase list
 * @apiVersion 0.0.1
 * @apiName PurchaseList
 * @apiGroup Purchase
 * @apiDescription Returns all purchases
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     {
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"id": 1, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"delivery": "Pick up at seller address"",
 *	},
 *     {
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"id": 1, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"delivery": "Door to door service / delivery",
 *	}
 */

 /**
 * @api {get} /purchase/?seller_id=ID Sales list
 * @apiVersion 0.0.1
 * @apiName SalesList
 * @apiGroup Purchase
 * @apiDescription Returns all sales
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     {
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"id": 1, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"delivery": "Pick up at seller address"",
 *	},
 *     {
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"id": 1, 
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"delivery": "Door to door service / delivery",
 *	}
 */

 /**
 * @api {post} /purchase/set-state-to-sent/ Purchase set state to sent
 * @apiVersion 0.0.1
 * @apiName PurchaseSetStateToSent
 * @apiGroup Purchase
 *
 * @apiParam {Integer} purchase_id -
 * @apiParam {Integer} type_sent_id -
 * @apiParam {String}  tracking_code optional	
 */

 /**
 * @api {get} /type-sent/ Type Sent list
 * @apiVersion 0.0.1
 * @apiName TypeSentList
 * @apiGroup TypeSent
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *       "id": 1, 
 *       "name": "Correo Argentino"
 *      },
 *      {
 *       "id": 2, 
 *       "name": "DHL"
 *      },
 */

 /**
 * @api {get} /purchase/?state=P|S|F|C|W|R Purchase state filters
 * @apiVersion 0.0.1
 * @apiName PurchaseStateFilters
 * @apiGroup Purchase
 *
 * @apiParam {String}   P      In Progress
 * @apiParam {String}   S      Sent
 * @apiParam {String}   F      Finished
 * @apiParam {String}   C      Canceled
 * @apiParam {String}   W      Conflict
 * @apiParam {String}   R      Received
 */

 /**
 * @api {get} /estimation/create-generic-estimation/ Estimation create
 * @apiVersion 0.0.1
 * @apiName EstimationCreate
 * @apiGroup Estimation
 * @apiDescription The format date MUST be YYYY-MM-DD
 *
 * @apiParam {Integer} product_id -
 * @apiParam {Integer} count -
 * @apiParam {String} buyer_id -
 * @apiParam {String} comments -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"product_id":1, "date_end":"2016-10-28", "count":1, "buyer_id":1,
 *     "comments": "some comment"}' 
 *     http://localhost:8000/api/v1/estimation/create-generic-estimation/
 */

/**
 * @api {get} /estimation/estimation-create-offer/ Create offer for estimation
 * @apiVersion 0.0.1
 * @apiName EstimationCreateOffer
 * @apiGroup Estimation
 *
 * @apiParam {Integer} seller_id -
 * @apiParam {Integer} estmation_id -
 * @apiParam {String} price -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"seller_id": 1, "estimation_id": 1, "price": "2.45"}'
 *     http://localhost:8000/api/v1/estimation/estimation-create-offer/
 */

 /**
 * @api {get} /purchase/ID/ Purchase detail
 * @apiVersion 0.0.1
 * @apiName PurchaseDetail
 * @apiGroup Purchase
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *		"buyer": "dev",
 *		"buyer_id": 1,
 *		"category": "Tegnologia", 
 *		"category_id": 1, 
 *		"count": 1, 
 *		"date_created": "2016-10-28", 
 *		"id": 1, 
 *		"price": "1500.00",
 *		"product": "Nokia", 
 *		"product_id": 1, 
 *		"sub_category": "Celulares", 
 *		"sub_category_id": 1,
 * 		"delivery": "Pick up at seller address",
 *		"seller_data": {"email": "", "name": "Alejandro Dev", "phone": null}
 *	}
 */

 /**
 * @api {get} /purchase/create-purchase/ Purchase create
 * @apiVersion 0.0.1
 * @apiName PurchaseCreate
 * @apiGroup Purchase
 *
 * @apiParam {Integer} product_id -
 * @apiParam {Integer} count -
 * @apiParam {Integer} seller_id -
 * @apiParam {Integer} buyer_id -
 * @apiParam {Integer} price_id -
 * @apiParam {Integer} offer_id -
 * @apiParam {String}  delivery P (Pick up at seller address) or D (Door to door service / delivery)
 * @apiParam {String}  city             -
 * @apiParam {String}  street           -
 * @apiParam {Integer} street_number    -
 * @apiParam {Integer} floor            -
 * @apiParam {Integer} unit             -
 * @apiParam {Integer} area_code        -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"product_id":1, "count": 1, "seller_id": 1, "delivery": "P", "buyer_id": 1}'
 *     http://localhost:8000/api/v1/purchase/create-purchase/
 */

 /**
 * @api {post} /purchase/seller-calification/ Seller calification
 * @apiVersion 0.0.1
 * @apiName SellerCalification
 * @apiGroup Purchase
 *
 * @apiParam {Integer}   calification      -
 * @apiParam {Integer}   purchase_id       -
 * @apiParam {String}    comments          -
 *
  * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"calification": 4, "purchase_id": "1"}'
 *     http://localhost:8000/api/v1/purchase/seller-calification/
 */

 /**
 * @api {post} /purchase/buyer-calification/ Buyer calification
 * @apiVersion 0.0.1
 * @apiName BuyerCalification
 * @apiGroup Purchase
 *
 * @apiParam {Integer}   calification      -
 * @apiParam {Integer}   purchase_id       -
 * @apiParam {String}    comments          -
 *
  * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"calification": 4, "purchase_id": "1"}'
 *     http://localhost:8000/api/v1/purchase/buyer-calification/
 */

 /**
 * @api {get} /question/?estimation_id=ESTIMATION_ID/ Get question list of a estimation
 * @apiVersion 0.0.1
 * @apiName QuestionList
 * @apiGroup Question
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *
 *    {
 *    "date_created": "2016-12-12",
 *     "estimation_id": 1,
 *     "id": 1,
 *     "question": "Esta que mola este producto?",
 *     "replies": [
 *       {
 *         "answer": "Yes, es un super producto",
 *         "date_created": "2016-12-12",
 *         "id": 1,
 *         "user": "admin"
 *       }
 *     ],
 *     "user": "Alejandro"
 *   },
 *   {
 *     "date_created": "2016-12-12",
 *     "estimation_id": 1,
 *     "id": 2,
 *     "question": "este producto apesta?",
 *     "replies": [
 *       {
 *         "answer": "Si la verdad que este producto apesta",
 *         "date_created": "2016-12-12",
 *         "id": 2,
 *         "user": "admin"
 *       }
 *     ],
 *     "user": "Alejandro"
 *   }
 */

 /**
 * @api {get} /question/questions-received/ Questions received
 * @apiVersion 0.0.1
 * @apiName QuestionsReceived
 * @apiGroup Question
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *
 *    {"estimation": 1, "id": 1, "question": "Some question", "user": 1}
 */

 /**
 * @api {get} /question//?user=USER_ID Questions created
 * @apiVersion 0.0.1
 * @apiName QuestionCreated
 * @apiGroup Question
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *
 *    {
 *        "date_created": "2017-05-15", 
 *        "estimation_id": 1, 
 *        "id": 1, 
 *        "question": "Some question", 
 *        "replies": [], 
 *        "user": "dev"
 *    }
 */

 /**
 * @api {post} /question/create-question/ Create Question
 * @apiVersion 0.0.1
 * @apiName QuestionCreate
 * @apiGroup Question
 *
 * @apiParam {Integer} estimation_id -
 * @apiParam {String} question -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"estimation_id": 1, "question": "este producto apesta?"}'
 *     http://localhost:8000/api/v1/question/create-question/
 */

/**
 * @api {post} /question/create-reply/ Create Reply
 * @apiVersion 0.0.1
 * @apiName ReplyCreate
 * @apiGroup Question
 *
 * @apiParam {Integer} question_id -
 * @apiParam {String} answer -
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"question_id": 2, "answer": "este producto es una masa"}'
 *     http://localhost:8000/api/v1/api/v1/question/create-reply/
 */

 /**
 * @api {get} /activitylog/user-action-logs/ User activity Log
 * @apiVersion 0.0.1
 * @apiName ActivityLog
 * @apiGroup ActivityLog
 *
 * @apiSuccess {Integer}  last_login      -
 * @apiSuccess {Integer}  last_purchase   -
 * @apiSuccess {Integer}  purchase_count  -
 * @apiSuccess {Integer}  last_sale       -
 * @apiSuccess {Integer}  sales_count     -
 */

/**
 * @api {get} /offer/ My offers
 * @apiVersion 0.0.1
 * @apiName OfferList
 * @apiGroup Offer
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *
 * {
 *     "category": "Tegnologia",
 *     "category_id": 1,
 *     "custom_estimation": {
 *       "buyer": 4,
 *       "category": 1,
 *       "comments": "sssddd",
 *       "count": 2,
 *       "date_end": "2016-12-22",
 *       "id": 1,
 *       "requested_product": "asbiii",
 *       "sub_category": 1
 *     },
 *     "date_created": "2016-12-07",
 *     "estimation": {
 *       "buyer": 1,
 *       "comments": "Comentarios",
 *       "count": 3,
 *       "date_end": "2016-12-22",
 *       "id": 5,
 *       "product": 2
 *     },
 *     "id": 3,
 *     "price": "6000.00",
 *     "sub_category": "Celulares",
 *     "sub_category_id": 1
 *   },
 *   {
 *     "category": "Tegnologia",
 *     "category_id": 1,
 *     "custom_estimation": "",
 *     "date_created": "2017-02-01",
 *     "estimation": {
 *       "buyer": 2,
 *       "comments": "Something",
 *       "count": 5,
 *       "date_end": "2016-12-11",
 *       "id": 1,
 *       "product": 1
 *     },
 *     "id": 4,
 *     "price": "2.45",
 *     "sub_category": "Celulares",
 *     "sub_category_id": 1
 *   }
 */
 
 /**
 * @api {post} /complaint/create-complaint/ Create complaint
 * @apiVersion 0.0.1
 * @apiName complaintCreate
 * @apiGroup Complaint
 *
 * @apiParam {Integer} purchase_id -
 * @apiParam {String} motive -
 * @apiParam {String} complaint_type D (Es distinto a lo que compre) B (Llego roto) L (Nunca llego) O (Otro)
 *
 * @apiExample {curl} Example usage:
 *     curl --dump-header - -H "Content-Type: application/json" -X POST --data 
 *     '{"motive": "alguno", "purchase_id": 5, "complaint_type": "D"}' 
 *      http://localhost:8000/api/v1/complaint/create-complaint/
 */
